#pragma once
#include "../../Common/HelperMacros.h"

/// /brief declares base class of a given class
///
/// To be used inside RTTI_DEFINE_XXXX bodies
/// Use only in class like types (what means also Assets )
/// will add all drawers of the base class, so place _it in the order you want to display varriables
#define RTTI_BASE(BaseClass)																			\
	static_assert(std::is_class_v<BaseClass>,															\
		SASSERT_ERROR_MESSAGE("RTTI_BASE", YK_STRINGIFY(BaseClass) "has to be a class")					\
	);																									\
	static_assert(std::is_base_of_v<BaseClass, Type>,													\
		SASSERT_ERROR_MESSAGE("RTTI_BASE", YK_STRINGIFY(BaseClass) "has to be a base for the type")		\
	);																									\
	static_assert(std::is_same_v<BaseClass, Type> == false,												\
		SASSERT_ERROR_MESSAGE("RTTI_BASE", YK_STRINGIFY(BaseClass) "cant derive from itself")			\
	);																									\
	RTTI_DESCRIPTOR_NAME->SetBaseType(rtti::GetClassDescriptor<BaseClass>())
	
/// /brief declares a property of a class
///
/// To be used inside RTTI_DEFINE_XXXX bodies
/// Use only in class like types (what means also Assets )
/// will place inspector drawer in the order you declare propertities
/// @returns created property, allows chain setup of property parameters if needed
/// for more information about property @see rtti::Property
#define RTTI_PROPERTY(propertyName)									\
	RTTI_DESCRIPTOR_NAME->AddProperty(								\
		rtti::GetTypeDescriptor<decltype(Type::propertyName)>(),	\
		YK_STRINGIFY(propertyName),									\
		offsetof(Type, propertyName),								\
		std::is_const_v<decltype(Type::propertyName)>,				\
		std::is_reference_v<decltype(Type::propertyName)>			\
)

/// /brief declares a member function of a class
///
/// To be used inside RTTI_DEFINE_XXXX bodies
/// Use only in class like types (what means also Assets )
/// @returns created function, allows chain setup of property parameters if needed
/// for more information about property @see rtti::FunctionBase
#define RTTI_MEMBER_FUNCTION(function)																					\
	RTTI_DESCRIPTOR_NAME->AddMemberFunction(new rtti::TMemberFunctionType{YK_STRINGIFY(function), &Type::function})		\

/// /brief adds a getter to a property @propertyName
///
/// To be used inside RTTI_DEFINE_XXXX bodies
/// Use only in class like types (what means also Assets )
/// Creates property if needed 
/// @returns added function
#define RTTI_PROPERTY_GETTER(propertyName, functionName)															\
	auto propertyName ## _Getter_ ## functionName = RTTI_DESCRIPTOR_NAME->FindFunctionByNativeName(#functionName);	\
	if(! propertyName ## _Getter_ ## functionName)																	\
	{																												\
		RTTI_MEMBER_FUNCTION(functionName);																			\
		propertyName ## _Getter_ ## functionName = RTTI_DESCRIPTOR_NAME->FindFunctionByNativeName(#functionName);	\
	}																												\
	RTTI_DESCRIPTOR_NAME->FindPropertyByNativeName(#propertyName)->SetOverridePropertyGetter(propertyName ## _Getter_ ## functionName);

/// /brief adds a setter to a property @propertyName
///
/// To be used inside RTTI_DEFINE_XXXX bodies
/// Use only in class like types (what means also Assets )
/// Creates property if needed 
/// @returns added function
#define RTTI_PROPERTY_SETTER(propertyName, functionName)															\
	auto propertyName ## _Setter_ ## functionName = RTTI_DESCRIPTOR_NAME->FindFunctionByNativeName(#functionName);	\
	if(! propertyName ## _Setter_ ## functionName)																	\
	{																												\
		RTTI_MEMBER_FUNCTION(functionName);																			\
		propertyName ## _Setter_ ## functionName = RTTI_DESCRIPTOR_NAME->FindFunctionByNativeName(#functionName);	\
	}																												\
	RTTI_DESCRIPTOR_NAME->FindPropertyByNativeName(#propertyName)->SetOverridePropertySetter(propertyName ## _Setter_ ## functionName);

/// /brief sets a default setter for the type
///
/// To be used inside RTTI_DEFINE_XXXX bodies
/// Creates property if needed 
/// @returns added function
/// TODO move to a file rttiTypeTraitsMacros
#define RTTI_DEFAULT_SETTER(functionName)																			\
	auto _DefaultSetter_ ## functionName = RTTI_DESCRIPTOR_NAME->FindFunctionByNativeName(#functionName);			\
	if(! _DefaultSetter_ ## functionName)																			\
	{																												\
		RTTI_MEMBER_FUNCTION(functionName);																			\
		_DefaultSetter_ ## functionName = RTTI_DESCRIPTOR_NAME->FindFunctionByNativeName(#functionName);			\
	}																												\
	RTTI_DESCRIPTOR_NAME->SetDefaultSetter(_DefaultSetter_ ## functionName);

/// /brief sets a default getter for the type
///
/// To be used inside RTTI_DEFINE_XXXX bodies
/// Creates property if needed 
/// @returns added function
/// TODO move to a file rttiTypeTraitsMacros
#define RTTI_DEFAULT_GETTER(functionName)																			\
	auto _DefaultGetter_ ## functionName = RTTI_DESCRIPTOR_NAME->FindFunctionByNativeName(#functionName);			\
	if(! _DefaultGetter_ ## functionName)																			\
	{																												\
		RTTI_MEMBER_FUNCTION(functionName);																			\
		_DefaultGetter_ ## functionName = RTTI_DESCRIPTOR_NAME->FindFunctionByNativeName(#functionName);			\
	}																												\
	RTTI_DESCRIPTOR_NAME->SetDefaultGetter(_DefaultGetter_ ## functionName);


/////////////// Drawing

/// /brief sets the name type will be displayed in editor with
///
/// To be used inside RTTI_DEFINE_XXXX bodies
/// TODO move to a file rttiTypeTraitsMacros
#define RTTI_DISPLAY_NAME(_Name)				\
	RTTI_DESCRIPTOR_NAME->SetDisplayName(_Name)

/// /brief sets default drawer type will use to draw _it's values
///
/// To be used inside RTTI_DEFINE_XXXX bodies
/// TODO move to a file rttiTypeTraitsMacros
#define RTTI_DRAWER(TypeDrawer, ...)			\
	RTTI_DESCRIPTOR_NAME->SetTypeDrawer<TypeDrawer>(__VA_ARGS__)

/// /brief sets default serializer type will use to serialize _it's values
///
/// To be used inside RTTI_DEFINE_XXXX bodies
/// TODO move to a file rttiTypeTraitsMacros
#define RTTI_SERIALIZER(TypeSerializer, ...) \
	RTTI_DESCRIPTOR_NAME->SetTypeSerializer<TypeSerializer>(__VA_ARGS__ )

/// /brief sets default category properties will be added to
///
/// To be used inside RTTI_DEFINE_XXXX bodies
/// Use only in class type declarations
#define RTTI_PROPERTY_CATEGORY(CategoryName) RTTI_DESCRIPTOR_NAME->SetPropertyCategory(CategoryName)


/// /brief sets default category properties will be added to
///
/// To be used inside RTTI_DEFINE_XXXX bodies
/// Use only in class type declarations
#define RTTI_TYPE_CATEGORY(CategoryName) RTTI_DESCRIPTOR_NAME->SetTypeCategory(CategoryName)

/// /brief sets tooltip to the class type
///
/// To be used inside RTTI_DEFINE_XXXX bodies
/// TODO move to a file rttiTypeTraitsMacros
#define RTTI_TOOLTIP(tooltip) RTTI_DESCRIPTOR_NAME->SetTooltip(tooltip)

/// /brief Forces this class to be treated as an abstract one
#define RTTI_FORCE_ABSTRACT() RTTI_DESCRIPTOR_NAME->SetAsAbstract(true)


/// /brief Allows VSL to use specific type
///
/// VSL - Visual Scripting Language
/// Only exposed types can be used for node creation purposes
/// Applies also to DynamicClass es
#define RTTI_EXPOSE_TO_VSL() RTTI_DESCRIPTOR_NAME->ExposeToVSL()

/// /brief Sets archetype of this class
///
/// Archetype is a class that could be used to initialize object of a given class
/// You can think of it as a set of parameters that define an object
/// Concept of Archetypes is similar to Prefabs but the difference is that Archetype doesn't have to be an instance of the class itself
#define RTTI_ARCHETYPE( _Archetype ) \
	static_assert(std::is_same_v<_Archetype, Type> == false, \
		SASSERT_ERROR_MESSAGE("RTTI_ARCHETYPE", YK_STRINGIFY(BaseClass) "cant be an archetype for itself")	\
	);																										\
	RTTI_DESCRIPTOR_NAME->SetArchetype( rtti::GetClassDescriptor<_Archetype>() );								\
	rtti::GetClassDescriptor<_Archetype>()->SetArchetypeFor( RTTI_DESCRIPTOR_NAME )

#define RTTI_INTERFACE(_Interface) \
	RTTI_DESCRIPTOR_NAME->AddInterface(rtti::GetInterfaceDescriptor<_Interface>())
