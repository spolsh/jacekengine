#include "engine.h"
#include "dx12_engine.h"
#include "vk_engine.h"
#include <filesystem>
#define DX12 0

Engine::Engine() : renderer(nullptr)
{
#if DX12
	renderer = new Dx12Engine();
#else
	renderer = new VulkanEngine();
#endif
}

Engine::~Engine()
{
	delete renderer;
}

void Engine::Init(std::span<float> geometry)
{
    std::filesystem::current_path(std::filesystem::current_path() / "../");
	renderer->Init(geometry);
}

void Engine::Run()
{

	renderer->Run();
}

void Engine::Cleanup()
{
	renderer->Cleanup();
}
