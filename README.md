# JACEK Engine

Jet Another Complicated Engine Kurwa (JACEK Engine) will be an open source 3D game engine developed using C++ and Vulkan.

**License:** MIT

**Authors:** The engine is developed by [Koło Naukowe Twórców Gier Polygon](https://kntgpolygon.pl/) - game development interest group at [Warsaw University of Technology](https://www.pw.edu.pl/).
