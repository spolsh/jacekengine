#pragma once
#include "../RttiCore.h"

namespace rtti
{
	namespace internal
	{
		template<typename Type>
		inline bool _SetFromString(Type* ptr, const char* valueString)
		{
			static std::stringstream stream;
			stream.str(valueString);

			stream >> *ptr;

			if (stream.fail())
			{
				stream.clear();
				return false;
			}

			return true;
		}
	}
	template<typename Type>
	inline bool SetFromString(Type* ptr, const char* valueString)
	{
		// by default there is no valid conversion
		return false;
	}

#define RTTI_DEFAULT_CONVERSION(Type)								\
	template<>														\
	inline bool SetFromString<Type>(Type* ptr, const char* valueString)	\
	{																\
		return internal::_SetFromString<Type>(ptr, valueString);	\
	}

	RTTI_DEFAULT_CONVERSION(bool);

	RTTI_DEFAULT_CONVERSION(uint8);
	RTTI_DEFAULT_CONVERSION(uint16);
	RTTI_DEFAULT_CONVERSION(uint32);
	RTTI_DEFAULT_CONVERSION(uint64);

	RTTI_DEFAULT_CONVERSION(int8);
	RTTI_DEFAULT_CONVERSION(int16);
	RTTI_DEFAULT_CONVERSION(int32);
	RTTI_DEFAULT_CONVERSION(int64);

	RTTI_DEFAULT_CONVERSION(float);
	RTTI_DEFAULT_CONVERSION(double);

	RTTI_DEFAULT_CONVERSION(String);
	RTTI_DEFAULT_CONVERSION(Path);
	template<> inline bool SetFromString<Name>(Name* ptr, const char* valueString)
	{													
		String s = ptr->GetString();
		return internal::_SetFromString<String>(&s, valueString);
	}
}
