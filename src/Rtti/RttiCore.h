#pragma once
// TODO make the file into pch

#include "../Common/ArrayUtility.h"
#include "../Common/StringUtility.h"
#include "../Common/MapUtility.h"
#include "../Common/Singleton.h"
#include "../Common/TypeIterator.h"

#include <functional>

#include "../Common/HelperMacros.h"

#include "../Common/Serialization/Serializer.h"
#include "../Common/Serialization/Serialization.h"
