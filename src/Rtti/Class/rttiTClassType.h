#pragma once
#include "rttiClassType.h"

namespace rtti
{
	/// /brief type template for ease of creation of nonabstract type objects
	template<typename Type>
	class TClassType : public ClassType
	{
	public:
		TClassType(const char* name)
			: ClassType(name, sizeof(Type))
		{
			static_assert(std::is_class<Type>::value
				, SASSERT_ERROR_MESSAGE("TClassType", "ClassType can be created only for classes")
				);
			SetAsAbstract(std::is_abstract<Type>::value);
			SetAsPolimorphic(std::is_polymorphic<Type>::value);
		}

		virtual void* CreateNewInPlace(void* memory) const override
		{
			if constexpr (std::is_default_constructible<Type>::value && std::is_abstract<Type>::value == false)
				return new(memory) Type{};
			else
				return nullptr;
		}
		virtual void* CreateNew() const override
		{
			if constexpr (std::is_default_constructible<Type>::value && std::is_abstract<Type>::value == false)
				return new Type;
			else
				return nullptr;
		}

		virtual void Destroy(void* object) const override
		{
			if constexpr (std::is_default_constructible<Type>::value && std::is_abstract<Type>::value == false)
				delete ((Type*)object);
		}

		virtual void DestroyInPlace(void* object) const override
		{
			if constexpr (std::is_default_constructible<Type>::value && std::is_abstract<Type>::value == false)
				((Type*)object)->~Type();
		}

	protected:
		virtual void* DuplicateNew(const void* original) const override
		{
			if constexpr (std::is_abstract<Type>::value == false)
			{
				if constexpr (std::is_copy_constructible<Type>::value)
				{
					return new Type(*((const Type*)original));// Call copy constructor
				}
			}
			
			return nullptr;
		}

		virtual void* MoveDuplicateNew(const void* original) const override
		{
			if constexpr (std::is_abstract<Type>::value == false)
			{
				if constexpr (std::is_move_constructible<Type>::value)
				{
					Type& t = *((Type*)original);
					return new Type(std::move(t));// Call move constructor
				}
			}

			return nullptr;
		}
	};
}