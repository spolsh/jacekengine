#include "rttiInterface.h"
#include "rttiClassType.h"

namespace rtti
{
	InterfaceType::InterfaceType(const char* name, uint16 size)
		: TypeBase(name, size)
	{
	}

	NO_DISCARD ClassType* InterfaceType::FindSubclassWithNativeName(const char* nativeName)
	{
		if (strcmp(GetNativeName(), nativeName) == 0)
		{
			return nullptr;
		}
		else
		{
			for (auto _it : GetAllDerivedIterator())
			{
				if (strcmp(_it->GetNativeName(), nativeName) == 0)
				{
					return _it;
				}
			}
		}

		return nullptr;
	}


	NO_DISCARD ClassType* InterfaceType::FindSubclassWithDisplayName(const char* displayName)
	{
		if (strcmp(GetDisplayName(), displayName) == 0)
		{
			return nullptr;
		}
		else
		{
			for (auto _it : GetAllDerivedIterator())
			{
				if (strcmp(_it->GetDisplayName(), displayName) == 0)
				{
					return _it;
				}
			}
		}

		return nullptr;
	}

	NO_DISCARD ClassType* InterfaceType::FindDirectSubclassWithNativeName(const char* nativeName)
	{
		if (strcmp(GetNativeName(), nativeName) == 0)
		{
			return nullptr;
		}
		else
		{
			for (auto _it : GetDirectlyDerivedIterator())
			{
				if (strcmp(_it->GetNativeName(), nativeName) == 0)
				{
					return _it;
				}
			}
		}

		return nullptr;
	}

	/// /brief searches over directly derived types one with a given display name
	NO_DISCARD ClassType* InterfaceType::FindDirectSubclassWithDisplayName(const char* displayName)
	{
		if (strcmp(GetDisplayName(), displayName) == 0)
		{
			return nullptr;
		}
		else
		{
			for (auto _it : GetDirectlyDerivedIterator())
			{
				if (strcmp(_it->GetDisplayName(), displayName) == 0)
				{
					return _it;
				}
			}
		}

		return nullptr;
	}
}