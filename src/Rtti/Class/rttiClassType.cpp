#include "rttiClassType.h"
//#include <Engine/Editor/BasicTypeDrawers.h>

namespace rtti
{

	ClassType::ClassType(const char* name, uint16 size)
		: TypeBase(name, size)
		, _isAbstract(false)
		, _baseType(nullptr)
		, _archetypeFor(nullptr)
	{
		SetTypeDrawer(this);
		SetTypeSerializer(this);
	}

	bool ClassType::DrawHeader(TypeDrawingContext& context) const
	{
        bool open = true;
#ifdef RTTI_ENABLE_DRAWING
		ImGuiTreeNodeFlags flags = ImGuiTreeNodeFlags_AllowItemOverlap
			| ImGuiTreeNodeFlags_DefaultOpen
			| ImGuiTreeNodeFlags_OpenOnArrow;

		ImGui::SetCursorPosX(ImGui::GetCursorPosX() + context.offset * editor::LABEL_OFFSET_STEP);

		if (context.drawHeader)
		{
			open = ImGui::CollapsingHeader(IMGUI_ID(context.GetDisplayName()), flags);
		}
#endif // RTTI_ENABLE_DRAWING
		return open;
	}
	bool ClassType::DrawType(TypeDrawingContext& context) const
	{
		++context.offset;
		return CompoundTypeDrawer::DrawType(context);
	}

	void ClassType::Serialize(YAML::Emitter& emitter, SerializationContext serializationContext) const
	{
#ifdef RTTI_ENABLE_SERIALIZATION
		emitter << YAML::BeginMap;
		if(IsPolimorphic())
			emitter << YAML::Key << "classType" << YAML::Value << _nativeName;
		
		for (auto _it : _properties)
		{
			SerializationContext propertyContext{serializationContext};
			propertyContext.offset += 1;
			propertyContext.property = _it.get();
			_it->Serialize(emitter, propertyContext);
		}

		emitter << YAML::EndMap;
#endif // RTTI_ENABLE_SERIALIZATION
	}

	void ClassType::Deserialize(const YAML::Node& node, SerializationContext serializationContext) const
	{
#ifdef RTTI_ENABLE_SERIALIZATION
		YK_ASSERT(node);

		for (auto _it : _properties)
		{
			_it->Deserialize(node, serializationContext);
		}
#endif // RTTI_ENABLE_SERIALIZATION
	}

}
