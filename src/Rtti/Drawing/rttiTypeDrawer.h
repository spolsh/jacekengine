#pragma once
#include "../RttiCore.h"

namespace rtti
{
	class FunctionTypeBase;

	/// /brief context for automatic drawing propertities in inspector or other places
	///
	/// in most cases you should use @rtti::Draw function instead manually filling in context parameters
	class TypeDrawingContext
	{
	public:
		TypeDrawingContext(void* objectStart, const char* imGuiId)
			: objectStart(objectStart)
			, property(nullptr)
			, type(nullptr)
			, imGuiId(imGuiId)
			, fieldName("")
			, readOnly(false)
			, drawHeader(true)
		{
			YK_ASSERT((objectStart, "TypeDrawingContext.objectStart can\'t be null, please assign a valid object address"));
		}

		const class TypeBase* type;

		/// property the current object is comming from, or nullptr if not applicable
		const class Property* property;

		/// adress of currently drawn object
		void* objectStart;
		
		/// imguiid prefix to make sure every call to imgui window will be unique
		const char* imGuiId;

		/// name of a field to be used if there is no property assigned (like in case of array entries)
		const char* fieldName;

		/// indentation before property is drawn
		int offset = 0;

		// should the header be drawn?
		// works only for the directly sent object
		bool drawHeader = true;

		// whould we display readOnly version of the widget?
		bool readOnly;

		/// /brief returns property name should be used to draw property
		const char* GetDisplayName() const;

		NO_DISCARD const FunctionTypeBase* GetPropertyGetter() const;

		NO_DISCARD const FunctionTypeBase* GetPropertySetter() const;

		NO_DISCARD const FunctionTypeBase* GetPropertyCondition() const;

	};

	/// Type drawers are used to define how given type will be drawn
	/// Properties are one kind of a drawer automatically added to drawer list from the type
	/// Not only properties are drawn, you can also draw messages, spaces, buttons, make that some properties will be drawn in one line and more
	class TypeDrawerBase
	{
	public:
		TypeDrawerBase()
			: _category("")
		{
		}

		// /brief displays type header
		//
		// By default it is just a 
		virtual bool DrawHeader(TypeDrawingContext& context) const;

		virtual void EndHeader(TypeDrawingContext& context) const {}

		// returns if parameter was changed
		virtual bool DrawType(TypeDrawingContext& context) const = 0;

		bool DrawValue(TypeDrawingContext& context) const;

		bool Draw(TypeDrawingContext& context, std::function<void()> afterLabelCall = []() {}) const;

		void SetPropertyCategory(const char* category) { _category = category; }
		const char* GetPropertyCategory() const { return _category; }

	protected:

		
		NO_DISCARD void* AllocateTempVarriable(TypeDrawingContext& context) const;

		void FreeTempVarriable(TypeDrawingContext& context, void* tempVarriable, bool changed) const;

	protected:
		const char*			_category;
	};

	/// /brief compound drawer is used to represent drawers that can store other drawers
	///
	/// Supports category filters
	/// TODO inheriting categories
	class CompoundTypeDrawer : public TypeDrawerBase
	{
	public:
		
		void AddDrawer(TypeDrawerBase* drawer);

		void RemoveDrawer(TypeDrawerBase* drawer);

		void PrepareCategories();

	protected:
		virtual bool DrawType(TypeDrawingContext& context) const override;

	protected:
		TDynArray<TypeDrawerBase*> _typeDrawingEntries;
		TDynArray<const char*> _categories;
	};
}
