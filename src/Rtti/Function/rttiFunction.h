#pragma once
#include "../Type/rttiTypeBase.h"

/// TODO move to some utility template file
template<typename Target, typename ListHead, typename... ListTails>
inline constexpr size_t GetTypeIndexInTemplateList()
{
	if constexpr (std::is_same<Target, ListHead>::value)
		return 0;
	else
		return 1 + GetTypeIndexInTemplateList<Target, ListTails...>();
}

namespace rtti
{
	/// /brief Context for rtti functions calls
	struct FunctionCallingContext
	{
		/// object adress for member functions
		/// if this is not a member function you can left it nullptr
		void* object = nullptr;

		/// object adress of the result expression value
		/// if function does not return anything you can left it nullptr
		void* result = nullptr;

		/// array of argument pointers
		TDynArray<void*> argumentPointers;
	};

	struct FunctionProperty
	{
		TypeBase* type;
		const char* displayName = "";
		const char* tooltip = "";
		bool isReference = false;
	};

	/// /brief base type for rtti functions
	class FunctionTypeBase
	{
	public:
		FunctionTypeBase(const char* functionName)
			: _nativeName(functionName)
			, _displayName(functionName)
			, _tooltip("")
			, _exposedToVSL(false)
		{

		}

		template<typename ReturnType, typename ...Args>
		bool HasSignature()
		{
			YK_ASSERT(false && "HasSignature is not implemented yet");
			// TODO
		}

		/// /brief invokes function,
		///
		/// provided context should contain data corresponding to stored function otherwise will assert
		virtual void Invoke(FunctionCallingContext& context) const = 0;

		/// /brief returns property the function returns. 
		virtual NO_DISCARD const FunctionProperty& GetResultProperty() const = 0;

		/// /brief returns property the function returns. 
		virtual NO_DISCARD FunctionProperty& GetResultProperty() = 0;

		/// /brief returns idth argument property
		virtual NO_DISCARD const FunctionProperty& GetArgumentProperty(size_t id) const = 0;

		/// /brief returns idth argument property
		virtual NO_DISCARD FunctionProperty& GetArgumentProperty(size_t id) = 0;

		/// /brief returns how many arguments function requires
		virtual NO_DISCARD size_t GetArgumentCount() const = 0;

		/// /brief returns a real name of the function
		///
		/// note can contain namespace too
		const NO_DISCARD char* GetNativeName() const { return _nativeName; }

		/// /brief returns name the function is displayed in editor with
		///
		/// by default display name is equal to native name
		const NO_DISCARD char* GetDisplayName() const { return _displayName; }

		/// /brief sets name the function is displayed in editor with
		///
		/// by default display name is equal to native name
		FunctionTypeBase& SetDisplayName(const char* displayName) { _displayName = displayName; return *this; }

		/// /brief returns tooltip for the function
		NO_DISCARD const char* GetTooltip() const { return _tooltip; }

		/// /brief sets tooltip for the function
		FunctionTypeBase& SetTooltip(const char* tooltip) { _tooltip = tooltip; return *this; }

		NO_DISCARD bool IsExposedToVSL() const { return _exposedToVSL; }

		void ExposeToVSL(bool b = true) { _exposedToVSL = b; }

	private:
		const char* _nativeName;
		const char* _displayName;

		const char* _tooltip;
		bool		_exposedToVSL;
	};

	template<typename Result, typename... Arguments >
	class TFunctionType : public FunctionTypeBase
	{
	public:
		TFunctionType(const char* functionName, Result(*ptr)(Arguments...))
			: FunctionTypeBase(functionName)
			, _ptr(ptr)
			, _argumentProperties({ (FunctionProperty{ GetTypeDescriptor<std::remove_reference_t<Arguments>>(), "Argument", "", std::is_reference_v<Arguments> })... })
			, _resultProperty(FunctionProperty{ GetTypeDescriptor<std::remove_reference_t<Result>>(), "Result", "", std::is_reference_v<Result> })
		{
		}
			
		virtual void Invoke(FunctionCallingContext& context) const override
		{
			if constexpr (std::is_same<void, Result>::value)
			{
				std::invoke(_ptr, (*((std::remove_reference_t<Arguments>*)context.argumentPointers[GetTypeIndexInTemplateList < Arguments, Arguments...>()]))...);
			}
			else
			{
				if (context.result)
				{
					*((std::remove_reference_t<Result>*)context.result) = std::invoke(_ptr, (*((std::remove_reference_t<Arguments>*)context.argumentPointers[GetTypeIndexInTemplateList < Arguments, Arguments...>()]))...);
				}
				else
				{
					std::invoke(_ptr, (*((std::remove_reference_t<Arguments>*)context.argumentPointers[GetTypeIndexInTemplateList < Arguments, Arguments...>()]))...);
				}
			}
		}

		Result Invoke(Arguments... arguments)
		{
			return _ptr(arguments...);
		}

		virtual NO_DISCARD const FunctionProperty& GetResultProperty() const override { return _resultProperty; }
		virtual NO_DISCARD const FunctionProperty& GetArgumentProperty(size_t id) const override { return _argumentProperties[id]; };
		virtual NO_DISCARD FunctionProperty& GetResultProperty() override { return _resultProperty; }
		virtual NO_DISCARD FunctionProperty& GetArgumentProperty(size_t id) { return _argumentProperties[id]; };

		virtual NO_DISCARD size_t GetArgumentCount() const override { return _argumentProperties.size(); }

	private:
		Result(*_ptr)(Arguments...);
		FunctionProperty _resultProperty;
		std::array<FunctionProperty, sizeof...(Arguments)> _argumentProperties;
	};

	template<typename ClassType, typename Result, typename... Arguments >
	class TMemberFunctionType : public FunctionTypeBase
	{
	public:
		TMemberFunctionType(const char* functionName, Result(ClassType::* ptr)(Arguments...))
			: FunctionTypeBase(functionName)
			, _ptr((Result(ClassType::*)(Arguments...) const)ptr)
			, _argumentProperties({ (FunctionProperty{ GetTypeDescriptor<std::remove_reference_t<Arguments>>(), "Argument", "", std::is_reference_v<Arguments> })... })
			, _resultProperty(FunctionProperty{ GetTypeDescriptor<std::remove_reference_t<Result>>(), "Result", "", std::is_reference_v<Result> })
		{
		}

		TMemberFunctionType(const char* functionName, Result(ClassType::* ptr)(Arguments...) const)
			: FunctionTypeBase(functionName)
			, _ptr(ptr)
			, _argumentProperties({ (FunctionProperty{ GetTypeDescriptor<std::remove_reference_t<Arguments>>(), "Argument", "", std::is_reference_v<Arguments> })... })
			, _resultProperty(FunctionProperty{ GetTypeDescriptor<std::remove_reference_t<Result>>(), "Result", "", std::is_reference_v<Result> })
		{
		}

		virtual void Invoke(FunctionCallingContext& context) const override
		{
			CallableInvoke callable;
			callable.object = (ClassType*)context.object;
			callable.ptr = _ptr;

			if constexpr (std::is_same<void, Result>::value)
			{
				std::invoke(callable, (*((std::remove_reference_t<Arguments>*)context.argumentPointers[GetTypeIndexInTemplateList < Arguments, Arguments...>()]))...);
			}
			else
			{
				if (context.result)
				{
					*((std::remove_reference_t<Result>*)context.result) = std::invoke(callable, (*((std::remove_reference_t<Arguments>*)context.argumentPointers[GetTypeIndexInTemplateList < Arguments, Arguments...>()]))...);
				}
				else
				{
					std::invoke(callable, (*((std::remove_reference_t<Arguments>*)context.argumentPointers[GetTypeIndexInTemplateList < Arguments, Arguments...>()]))...);
				}
			}
		}

		Result Invoke(ClassType& object, Arguments... arguments)
		{
			return (object.*_ptr)(arguments...);
		}

		virtual NO_DISCARD const FunctionProperty& GetResultProperty() const override { return _resultProperty; }
		virtual NO_DISCARD const FunctionProperty& GetArgumentProperty(size_t id) const override { return _argumentProperties[id]; };
		virtual NO_DISCARD FunctionProperty& GetResultProperty() override { return _resultProperty; }
		virtual NO_DISCARD FunctionProperty& GetArgumentProperty(size_t id) { return _argumentProperties[id]; };

		virtual NO_DISCARD size_t GetArgumentCount() const override { return _argumentProperties.size(); }

	private:
		struct CallableInvoke
		{
			ClassType* object;
			Result(ClassType::* ptr)(Arguments...) const;
			Result operator() (Arguments... args) const noexcept
			{
				return (object->*ptr)(args...);
			}
		};


	private:
		Result(ClassType::* _ptr)(Arguments...) const;

		FunctionProperty _resultProperty;
		std::array<FunctionProperty, sizeof...(Arguments)> _argumentProperties;
	};

}
