#pragma once
#include <assert.h>
#include "Macro_VAARGS.h"


#define YK_FLAG(flag) 1 << (flag)
#define YK_CHECK_FLAG(value, flag) ( ((value) & (flag)) == (flag))
#define YK_CHECK_FLAG_CAST(value, flag, castType) ( ((value) & (castType)(flag)) == (castType)(flag))

#define YK_STRINGIFY_DETAIL(String) #String
#define YK_STRINGIFY(String) YK_STRINGIFY_DETAIL(String) 

#define YK_ASSERT(x) assert((x))
#define YK_CHECK(x) assert((x) != nullptr); (x)

#define SASSERT_ERROR_MESSAGE( _Where, _Message) _Where ": " _Message "; " YK_STRINGIFY(__LINE__) ", " __FILE__ 

/* This will let macros expand before concating them */
#define PRIMITIVE_CAT(x, y) x ## y
#define CAT(x, y) PRIMITIVE_CAT(x, y)

#define PASS1(a) a
#define PASS2(a,b) a,b
#define PASS3(a,b,c) a,b,c
#define PASS4(a,b,c,d) a,b,c,d
#define PASS5(a,b,c,d,e) a,b,c,d,e
#define PASS6(a,b,c,d,e,f) a,b,c,d,e,f

#define PASS(...) PASS1(CAT(PASS, VAR_COUNT(__VA_ARGS__)))

#define NO_DISCARD [[nodiscard]]
#define NO_RETURN [[noreturn]]
#define FALLTHROUGH [[fallthrough]]
#define LIKELY [[likely]]
#define UNLIKELY [[unlikely]]

#define FORCEINLINE __forceinline
#define RESTRICT restrict

#define ASSUME(_expression) [[assume( _expression )]]

#define DEPRECATED(_message) [[deprecated( _message )]]

#ifdef _MSC_VER
#if _MSC_VER >= 1929
	#define NO_UNIQUE_ADDRESS [[msvc::no_unique_address]]
#endif
#else
	#define NO_UNIQUE_ADDRESS [[no_unique_address]]		
#endif // _MSC_VER	

/// because macros have a problem with commas and < > brackets we need a compact way to avoid the problem while writing the type signature
#define TEMPLATED_TYPE( T, ... ) T<__VA_ARGS__>

#define COMBINE_INNER_1( _A)									_A 
#define COMBINE_INNER_2( _A, _B)								_A ## _B
#define COMBINE_INNER_3( _A, _B, _C)							_A ## _B ## _C
#define COMBINE_INNER_4( _A, _B, _C, _D)						_A ## _B ## _C ## _D
#define COMBINE_INNER_5( _A, _B, _C, _D, _E)					_A ## _B ## _C ## _D ## _E
#define COMBINE_INNER_6( _A, _B, _C, _D, _E, _F)				_A ## _B ## _C ## _D ## _E ## _F
#define COMBINE_INNER_7( _A, _B, _C, _D, _E, _F, _G)			_A ## _B ## _C ## _D ## _E ## _F ## _G
#define COMBINE_INNER_8( _A, _B, _C, _D, _E, _F, _G, _H)		_A ## _B ## _C ## _D ## _E ## _F ## _G ## _H
#define COMBINE_INNER_9( _A, _B, _C, _D, _E, _F, _G, _H, _I)	_A ## _B ## _C ## _D ## _E ## _F ## _G ## _H ## _I

#define COMBINE(...) EXPAND(CAT( COMBINE_INNER_, VAR_COUNT(__VA_ARGS__))(__VA_ARGS__))

/* This will pop the last argument off */
#define POP_LAST_1(x1)
#define POP_LAST_2(x1, x2) x1
#define POP_LAST_3(x1, x2, x3) x1, x2
#define POP_LAST_4(x1, x2, x3, x4) x1, x2, x3
#define POP_LAST_5(x1, x2, x3, x4, x5) x1, x2, x3, x4
#define POP_LAST_6(x1, x2, x3, x4, x5, x6) x1, x2, x3, x4, x5
#define POP_LAST_7(x1, x2, x3, x4, x5, x6, x7) x1, x2, x3, x4, x5, x6
#define POP_LAST_8(x1, x2, x3, x4, x5, x6, x7, x8) x1, x2, x3, x4, x5, x6, x7

#define POP_LAST(...) EXPAND( CAT(POP_LAST_, VAR_COUNT(__VA_ARGS__))(__VA_ARGS__) )

/* This will return the last argument */
#define LAST_1(x1) x1
#define LAST_2(x1, x2) x2
#define LAST_3(x1, x2, x3) x3
#define LAST_4(x1, x2, x3, x4) x4
#define LAST_5(x1, x2, x3, x4, x5) x5
#define LAST_6(x1, x2, x3, x4, x5, x6) x6
#define LAST_7(x1, x2, x3, x4, x5, x6, x7) x7
#define LAST_8(x1, x2, x3, x4, x5, x6, x7, x8) x8

#define LAST(...) EXPAND(CAT(LAST_, VAR_COUNT(__VA_ARGS__))(__VA_ARGS__))


#define BUILD_TEMPLATED_TYPE_1(Class, Arg1) \
	Class< Arg1 >
#define BUILD_TEMPLATED_TYPE_2(Class, Arg1, Arg2) \
	Class< Arg1, Arg2 >
#define BUILD_TEMPLATED_TYPE_3(Class, Arg1, Arg2, Arg3) \
	Class< Arg1, Arg2, Arg3 >
#define BUILD_TEMPLATED_TYPE_4(Class, Arg1, Arg2, Arg3, Arg4) \
	Class< Arg1, Arg2, Arg3, Arg4 >
#define BUILD_TEMPLATED_TYPE_5(Class, Arg1, Arg2, Arg3, Arg4, Arg5) \
	Class< Arg1, Arg2, Arg3, Arg4, Arg5 >
#define BUILD_TEMPLATED_TYPE_6(Class, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6) \
	Class< Arg1, Arg2, Arg3, Arg4, Arg5, Arg6 >
#define BUILD_TEMPLATED_TYPE_7(Class, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7) \
	Class< Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7 >
#define BUILD_TEMPLATED_TYPE_8(Class, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8) \
	Class< Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8 >

#define BUILD_TEMPLATED_TYPE(Class, ...)	\
	EXPAND( CAT(BUILD_TEMPLATED_TYPE_, VAR_COUNT(__VA_ARGS__))(Class, __VA_ARGS__) )

#define BUILD_TEMPLATED_NAME(Class, ...)	\
	EXPAND( YK_STRINGIFY( BUILD_TEMPLATED_TYPE(Class, __VA_ARGS__ ) ) )
