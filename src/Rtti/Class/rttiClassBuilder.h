#pragma once
#include "rttiTClassType.h"
#include "../Type/rttiTypeBuilder.h"

namespace rtti
{
	class ClassType;

	class ClassBuilder
	{
	public:
		ClassBuilder(ClassType* typeDescriptor, std::function<void(ClassType*)> initializer)
		{
			initializer(typeDescriptor);
		}
	};

	namespace Internal {
		// Function used to prevent compiler from not instantiating a static varriable and its constructor
		template<typename T>
		void ___FakeCallFunction() {}
	}

	template<typename Type>
	void SerializePolimorphic(YAML::Emitter& emitter, const Type& objToSerialize)
	{
#ifdef RTTI_ENABLE_SERIALIZATION
		rtti::SerializationContext serializationContext((Type*)&objToSerialize);
		RE_CHECK(objToSerialize.GetType())->Serialize(emitter, serializationContext);
#endif
	}

	template<typename Type>
	void SerializePolimorphic(YAML::Emitter& emitter, rtti::SerializationContext serializationContext)
	{
#ifdef RTTI_ENABLE_SERIALIZATION
		auto ptr = (Type*)serializationContext.objectStart;

		if (ptr == nullptr)
		{
			YamlMapSerializer serializer(emitter);
			serializer.SerializeElement("classType", "nullptr");
			return;
		}
		RE_CHECK(ptr->GetType())->Serialize(emitter, serializationContext);
#endif
	}

	template<typename Type>
	Type* DeserializePolimorphic(const YAML::Node& node)
	{
#ifdef RTTI_ENABLE_SERIALIZATION
		RE_ASSERT(node.IsMap());
		String nativeType = node["classType"].as<String>();
		if (nativeType == "nullptr")
			return nullptr;

		rtti::ClassType* rttiType = rtti::GetClassDescriptor<Type>();
		RE_ASSERT(rttiType);

		rtti::ClassType* deserializedType = rttiType->FindSubclassWithNativeName(nativeType.c_str());
		Type* newPtr = (Type*)deserializedType->CreateNew();

		rtti::SerializationContext serializationContext((Type*)newPtr);
		deserializedType->Deserialize(node, serializationContext);

		return newPtr; 
#else
        return nullptr;
#endif
	}

	template<typename Type>
	void SerializeToFilePolimorphic(const Path& filePath, const Type& objToSerialize)
	{
#ifdef RTTI_ENABLE_SERIALIZATION
		rtti::TypeBase* type = objToSerialize.GetType();
		rtti::SerializationContext serializationContext((Type*)&objToSerialize);
		RE_CHECK(type)->SerializeToFile(filePath, serializationContext);
#endif
	}

	template<typename Type>
	void DeserializeFromFilePolimorphic(const Path& filePath, Type& objToDeserialize)
	{
#ifdef RTTI_ENABLE_SERIALIZATION
		rtti::TypeBase* type = objToDeserialize.GetType();
		rtti::SerializationContext serializationContext(&objToDeserialize);
		RE_CHECK(type)->DeserializeFromFile(filePath, serializationContext);
#endif
	}

	template<typename Type>
	void SerializeToStringPolimorphic(const Path& filePath, const Type& objToSerialize)
	{
#ifdef RTTI_ENABLE_SERIALIZATION
		auto type = objToSerialize.GetType();
		rtti::SerializationContext serializationContext((Type*)&objToSerialize);
		RE_CHECK(type)->SerializeToString(filePath, serializationContext);
#endif
	}

	template<typename Type>
	void DeserializeFromStringPolimorphic(const Path& filePath, Type& objToDeserialize)
	{
#ifdef RTTI_ENABLE_SERIALIZATION
		auto type = objToDeserialize.GetType();
		rtti::SerializationContext serializationContext(&objToDeserialize);
		RE_CHECK(type)->DeserializeFromString(filePath, serializationContext);
#endif
	}

	template<typename T>
	String BuildTemplatedArgsName()
	{
		rtti::TypeBase* t = rtti::GetTypeDescriptor<T>();
		return t->GetNativeName();
	}

	template<typename T, typename T2, typename... Args>
	String BuildTemplatedArgsName()
	{
		rtti::TypeBase* t = rtti::GetTypeDescriptor<T>();
		return BuildTemplatedArgsName<T>() + BuildTemplatedArgsName<T2, Args...>();
	}

	template<typename... Args>
	String BuildTemplatedName(String className)
	{
		return className + "<" + BuildTemplatedArgsName<Args...>() + ">" ;
	}

}

#define EMPTY

#define INTERNAL_RTTI_TYPE_STANDARD_FUNCTIONS(Virtual) \
		Virtual EXPAND(NO_DISCARD) const rtti::ClassType* GetType() const { return GetStaticType(); }\
		static EXPAND(NO_DISCARD) rtti::ClassType* GetStaticType();\
		template<typename _Ty> EXPAND(NO_DISCARD) const _Ty* As() const { return (const _Ty*)As(rtti::GetClassDescriptor<_Ty>()); }\
		EXPAND(NO_DISCARD) const void* As(const rtti::ClassType* type) const { return GetType()->IsA(type) ? this : nullptr; }\
		template<typename _Ty> EXPAND(NO_DISCARD) _Ty* As() { return (_Ty*)As(rtti::GetClassDescriptor<_Ty>()); }\
		EXPAND(NO_DISCARD) void* As(rtti::ClassType* type) { return GetType()->IsA(type) ? this : nullptr; }\
		

#define BEFRIEND_TYPE_DESCRIPTOR(Class)\
	private:\
		friend class rtti::ClassBuilder;\
		friend struct rtti::Internal::TypeDescriptor<Class>;\

/// /brief Declares reflected class body propertities with support for dynamic type detection
/// 
/// Put _it inside class declaration
/// @Class is a class name (without namespaces)
#define RTTI_DECLARE_POLYMORPHIC_CLASS(Class)\
	private:\
		static rtti::ClassBuilder RTTI_BUILDER_NAME;\
		BEFRIEND_TYPE_DESCRIPTOR(Class)\
	public:\
		INTERNAL_RTTI_TYPE_STANDARD_FUNCTIONS(virtual)

		
/// /brief Declares reflected class body propertities for non polimorphic types
/// 
/// Put _it inside class declaration
/// @Class is a class name (without namespaces)
/// 
/// Also used for templated types specialization
#define RTTI_DECLARE_CLASS(Class)\
	private:\
		static rtti::ClassBuilder RTTI_BUILDER_NAME;\
		BEFRIEND_TYPE_DESCRIPTOR(Class)\
	public:\
		INTERNAL_RTTI_TYPE_STANDARD_FUNCTIONS(EXPAND(EMPTY))


/// /brief Define a class type propertities
/// 
/// @Class should be with potential namespaces
/// 
/// inside _Body expression define propertities
/// @warrning do not put this macro inside any namespace, place _it in cpp file
/// 
/// Also used for templated types specialization
#define RTTI_DEFINE_CLASS(Class, _BODY)																			\
rtti::ClassBuilder Class::RTTI_BUILDER_NAME(Class::GetStaticType(), [](rtti::ClassType*) {});					\
rtti::ClassType* Class::GetStaticType()																			\
{																												\
	static rtti::TClassType<std::remove_reference_t<Class>> RTTI_DESCRIPTOR_NAME(YK_STRINGIFY(Class));									\
	static rtti::ClassBuilder RTTI_BUILDER_NAME(&RTTI_DESCRIPTOR_NAME, [](rtti::ClassType* RTTI_DESCRIPTOR_NAME)\
	{																											\
		using Type = Class;																						\
		_BODY																									\
		RTTI_DESCRIPTOR_NAME->PrepareCategories();																\
		RTTI_DESCRIPTOR_NAME->FinalizeInitialization();															\
	});																											\
	return &RTTI_DESCRIPTOR_NAME;																				\
}																												\
namespace rtti { namespace Internal \
{\
	template<> inline void ___FakeCallFunction<Class>() { std::cout << Class::GetStaticType()->GetDisplayName() << "\n"; } \
}}

/// /brief
///
/// EXPERIMENTAL
#define RTTI_DUPLICATE_THIS(virt, BaseClass, additionalInitialization)\
	RTTI_DUPLICATE(virt, BaseClass, \
		{\
			duplicated->_this = duplicated; \
			additionalInitialization\
		})

/// /brief Externaly registers a class type into the reflection system
/// 
/// in case you can't (or for some reason don't want to) modify class declaration you can register the function externaly
///		downside to that approach is lack of usefull functions and a fact that you need to do type declaration in header file -> longer compilation
/// 
/// Type is not instantiated until first call appears
/// 
/// inside _Body expression define propertities
/// 
/// Place in a header file
/// @warrning do not put this macro inside any namespace
/// 
/// Also used for templated types specialization
#define RTTI_DEFINE_EXTERNAL_CLASS(_Type, _Body)\
template<>\
struct rtti::Internal::TypeDescriptor<_Type>\
{\
	TypeBase* operator()()\
	{\
		using Type = _Type;\
		static TClassType<std::remove_reference_t<Type>>	RTTI_DESCRIPTOR_NAME(YK_STRINGIFY(_Type));\
		static ClassBuilder	RTTI_BUILDER_NAME(&RTTI_DESCRIPTOR_NAME, [](ClassType* RTTI_DESCRIPTOR_NAME)\
		{\
			_Body \
			RTTI_DESCRIPTOR_NAME->PrepareCategories();\
			RTTI_DESCRIPTOR_NAME->FinalizeInitialization();\
		});\
		return &RTTI_DESCRIPTOR_NAME;\
	}\
};

/// /brief Externaly registers a templated class type into the reflection system
/// 
/// Use in a header file
/// You need to put template< *List of template types* > before this definition
/// 
/// in case you can't (or for some reason don't want to) modify class declaration you can register the function externaly
///		downside to that approach is lack of usefull functions and a fact that you need to do type declaration in header file -> longer compilation
/// 
/// Type is not instantiated until first call appears
/// 
/// 
/// inside _Body expression define propertities
/// @warrning do not put this macro inside any namespace
#define RTTI_DEFINE_EXTERNAL_TEMPLATE_CLASS(_Type, ...)\
struct rtti::Internal::TypeDescriptor<EXPAND( BUILD_TEMPLATED_TYPE(_Type, POP_LAST(__VA_ARGS__) ) )>\
{\
	TypeBase* operator()()\
	{\
		using Type = EXPAND( BUILD_TEMPLATED_TYPE(_Type, POP_LAST(__VA_ARGS__) ) );\
		static rtti::TClassType<std::remove_reference_t<Type>>	RTTI_DESCRIPTOR_NAME(rtti::BuildTemplatedName<POP_LAST(__VA_ARGS__)> (YK_STRINGIFY(_Type) ).c_str());\
		static rtti::ClassBuilder	RTTI_BUILDER_NAME(&RTTI_DESCRIPTOR_NAME, [](ClassType* RTTI_DESCRIPTOR_NAME)\
		{\
			LAST(__VA_ARGS__) \
			RTTI_DESCRIPTOR_NAME->PrepareCategories();\
			RTTI_DESCRIPTOR_NAME->FinalizeInitialization();\
		});\
		return &RTTI_DESCRIPTOR_NAME;\
	}\
};

/// /brief Declares reflected class body propertities for templated types
/// 
/// You need to put template< *List of template types* > before this definition
/// 
/// Put _it inside class declaration
/// @Class is a class name (without namespaces)
#define RTTI_DECLARE_TEMPLATED_CLASS(Class)\
	private:\
		BEFRIEND_TYPE_DESCRIPTOR(Class)\
	public:\
		INTERNAL_RTTI_TYPE_STANDARD_FUNCTIONS(EXPAND(EMPTY))



/// /brief Define a class type propertities
/// 
/// Class should be with potential namespaces
/// 
/// inside _Body expression define propertities
/// @warrning do not put this macro inside any namespace, place _it in a header file
#define RTTI_DEFINE_TEMPLATED_CLASS(Class, ... )																\
inline rtti::ClassType* EXPAND( BUILD_TEMPLATED_TYPE(Class, POP_LAST(__VA_ARGS__) ) )::GetStaticType()			\
{																												\
	using Type = EXPAND( BUILD_TEMPLATED_TYPE(Class, POP_LAST(__VA_ARGS__) ) );									\
	static rtti::TClassType<std::remove_reference_t<Type>> RTTI_DESCRIPTOR_NAME(rtti::BuildTemplatedName<POP_LAST(__VA_ARGS__)> (YK_STRINGIFY(Class) ).c_str() );		\
	static rtti::ClassBuilder RTTI_BUILDER_NAME(&RTTI_DESCRIPTOR_NAME, [](rtti::ClassType* RTTI_DESCRIPTOR_NAME)\
	{																											\
		LAST(__VA_ARGS__)																						\
		RTTI_DESCRIPTOR_NAME->PrepareCategories();																\
		RTTI_DESCRIPTOR_NAME->FinalizeInitialization();															\
	});																											\
	return &RTTI_DESCRIPTOR_NAME;																				\
}

// forces rtti system to acknowledge existence of specific template instance
// usefull for types that are declared only with using 
// allows to search for derived types
#define RTTI_DEFINE_CONCRETE_CLASS( ClassName ) // TODO 

// TODO:
//		* RTTI_DECLARE_TEMPLATED_CLASS_SPECIALIZATION,
//		* RTTI_DEFINE_TEMPLATED_CLASS_SPECIALIZATION,
//		* RTTI_DEFINE_EXTERNAL_TEMPLATED_CLASS_SPECIALIZATION
// ugh those names are getting quite long
// TODO Templated asset Asset version
