#pragma once

#include <d3d12.h>

#include "dx12_utils.h"
#include "dx12_command_list_allocator.h"

class Dx12CommandList
{
public:
	Dx12CommandList(ID3D12Device* device, Dx12CommandListAllocator* allocator, D3D12_COMMAND_LIST_TYPE type = D3D12_COMMAND_LIST_TYPE_DIRECT) : type_(type)
	{
		if (allocator == nullptr || type != allocator->GetType())
		{
			exit(1);
		}

		// We would like to create command list here, to be aligned with what happens at the API level
		auto result = device->CreateCommandList(0, type, allocator->GetRaw(), nullptr, IID_PPV_ARGS(&command_list_));
		if (FAILED(result))
		{
			exit(1);
		}
		command_list_->Close();
	}
	~Dx12CommandList()
	{
		SafeRelease(command_list_);
	}
	ID3D12GraphicsCommandList* GetRaw()
	{
		return command_list_;
	}
	void Reset()
	{
	}
private:
	D3D12_COMMAND_LIST_TYPE type_;
	ID3D12GraphicsCommandList* command_list_;
};
