#pragma once
#include "ArrayUtility.h"

template<typename Type, typename IteratorType = void>
class TypeIterator
{
public:
	TypeIterator(const Type& objects)
		: _objects(&objects)
	{
	}

	auto begin() { return IteratorType(_objects->begin()); }
	auto end() { return IteratorType(_objects->end()); }
	auto cbegin() const { return IteratorType(_objects->begin()); }
	auto cend() const { return IteratorType(_objects->end()); }
	auto begin() const { return IteratorType(_objects->begin()); }
	auto end() const { return IteratorType(_objects->end()); }

private:
	const Type* _objects;
};

template<typename Type>
class TypeIterator<Type, void>
{
public:
	TypeIterator(const Type& objects)
		: _objects(&objects)
	{
	}

	auto begin() { return _objects->begin(); }
	auto end() { return _objects->end(); }
	auto cbegin() const { return _objects->begin(); }
	auto cend() const { return _objects->end(); }
	auto begin() const { return _objects->begin(); }
	auto end() const { return _objects->end(); }

private:
	const Type* _objects;
};

template<typename Iterator>
class RangeIterator
{
public:
	RangeIterator(const std::pair<Iterator, Iterator>& it)
		:_iterator(it)
	{
	}

	auto begin() { return _iterator.first; }
	auto end() { return _iterator.second; }

private:
	std::pair<Iterator, Iterator> _iterator;
};