#include "rttiTypeBase.h"
#include "../Serialization/rttiSerialization.h"
#include "../Drawing/rttiTypeDrawer.h"

namespace rtti
{
	TypeBase::TypeBase(const char* name, uint16 size)
		: _displayName(name)
		, _nativeName(name)
		, _typeDrawer(nullptr)
		, _typeSerializer(nullptr)
		, _size(size)
		, _tooltip("")
		, _defaultSetter(nullptr)
		, _defaultGetter(nullptr)
		, _defaultCondition(nullptr)
		, _isTypeSerializerAReference(false)
		, _isTypeDrawerAReference(false)
		, _isAReference(false)
		, _exposedToVSL(false)
	{
		GetAllTypes().push_back(this);
	}
	TypeBase::~TypeBase()
	{
		if (_typeDrawer && !_isTypeDrawerAReference)
			delete _typeDrawer;

		if (_typeSerializer && !_isTypeSerializerAReference)
			delete _typeSerializer;

		auto found = std::find(GetAllTypes().begin(), GetAllTypes().end(), this);
		if (found != GetAllTypes().end())
		{
			GetAllTypes().erase(found);
		}
	}
	void TypeBase::FinalizeInitialization()
	{
	}
	void TypeBase::Serialize(YAML::Emitter& emitter, SerializationContext context) const
	{
		_typeSerializer->Serialize(emitter, context);
	}
	void TypeBase::Deserialize(const YAML::Node& node, SerializationContext context) const
	{
		_typeSerializer->Deserialize(node, context);
	}
	bool TypeBase::SerializeToFile(const Path& filePath, const SerializationContext& context) const
	{
#ifdef RTTI_ENABLE_SERIALIZATION
		YAML::Emitter emitter;
		_typeSerializer->Serialize(emitter, context);
		return ::SerializeToFile( emitter, filePath);
#else
        return false;
#endif
	}

	bool TypeBase::DeserializeFromFile(const Path& filePath, const SerializationContext& context) const
	{
#ifdef RTTI_ENABLE_SERIALIZATION
		YAML::Node node = ::DeserializeFromFile(filePath);

		if (node)
		{
			_typeSerializer->Deserialize(node, context);
			return true;
		}
#endif
		return false;
	}

	bool TypeBase::SerializeToString(String& serializationTarget, const SerializationContext& context) const
	{
#ifdef RTTI_ENABLE_SERIALIZATION
		YAML::Emitter emitter;
		_typeSerializer->Serialize(emitter, context);

		return ::SerializeToString( emitter, serializationTarget); 
#endif
        return false;
	}

	bool TypeBase::DeserializeFromString(const std::string_view& deserializationTarget, const SerializationContext& context) const
	{
#ifdef RTTI_ENABLE_SERIALIZATION
		YAML::Node node = ::DeserializeFromString(deserializationTarget);

		if (node)
		{
			_typeSerializer->Deserialize(node, context);

			return true;
		}
#endif
		return false;
	}

}
