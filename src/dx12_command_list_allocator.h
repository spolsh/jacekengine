#pragma once

#include <d3d12.h>

#include "dx12_utils.h"

class Dx12CommandListAllocator
{
public:
	Dx12CommandListAllocator(ID3D12Device* device, D3D12_COMMAND_LIST_TYPE type = D3D12_COMMAND_LIST_TYPE_DIRECT) : type_(type)
	{
		auto result = device->CreateCommandAllocator(type, IID_PPV_ARGS(&allocator_));
		if (FAILED(result))
		{
			exit(1);
		}
	}
	~Dx12CommandListAllocator()
	{
		SafeRelease(allocator_);
	}
	ID3D12CommandAllocator* GetRaw()
	{
		return allocator_;
	}
	D3D12_COMMAND_LIST_TYPE GetType()
	{
		return type_;
	}
	void Reset()
	{
		allocator_->Reset();
	}
private:
	D3D12_COMMAND_LIST_TYPE type_;
	ID3D12CommandAllocator* allocator_;
};
