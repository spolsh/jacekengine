#pragma once
#include "../Type/rttiTypeBase.h"
#include "../Drawing/rttiTypeDrawer.h"
#include "../Serialization/rttiSerialization.h"

namespace rtti
{
	template<typename KeyType, typename ElementType>
	class TTreeMapType : public TypeBase, public TypeDrawerBase, public ITypeSerializerBase
	{
	public:
		using MapType = TTreeMap<KeyType, ElementType>;
		using PairType = std::pair<KeyType, ElementType>;

		TTreeMapType(const char* name)
			: TypeBase(name, sizeof(MapType))
		{
			SetTypeDrawer(this);
			SetTypeSerializer(this);
		}

		const TypeBase* GetMapElementType() const { return GetTypeDescriptor<ElementType>(); }
		
		const TypeBase* GetMapKeyType() const { return GetTypeDescriptor<KeyType>(); }

		const TypeBase* GetMapPairType() const { return GetTypeDescriptor<PairType>(); }

		void* GetMapElementAdress(size_t id, void* arrayStart) const
		{
			MapType* array = (MapType*)arrayStart;

			auto iterator = array->begin();
			for (size_t i = 0; i < id; ++i)
				++iterator;
			return &(iterator->second);
		}
		const void* GetMapKeyAdress(size_t id, void* arrayStart) const
		{
			MapType* array = (MapType*)arrayStart;

			auto iterator = array->begin();
			for (size_t i = 0; i < id; ++i)
				++iterator;
			return &(iterator->first);
		}
		const void* GetMapPairAdress(size_t id, void* arrayStart) const
		{
			MapType* array = (MapType*)arrayStart;

			auto iterator = array->begin();
			for (size_t i = 0; i < id; ++i)
				++iterator;
			return &(*iterator);
		}

		size_t GetMapSize(void* arrayStart) const
		{
			MapType* array = (MapType*)arrayStart;
			return array->size();
		}

		virtual void* CreateNew() const override
		{
			return new MapType();
		}

		virtual void  Destroy(void* object) const override
		{
			delete ((MapType*)object);
		}
		virtual void DestroyInPlace(void* object) const override
		{
			((MapType*)object)->~Type();
		}

	protected:
		
		virtual void* CreateNewInPlace(void* memory) const override
		{
			if constexpr (std::is_default_constructible<ElementType>::value && std::is_abstract<ElementType>::value == false)
				return new(memory) MapType{};
			else
				return nullptr;
		}
		virtual void* DuplicateNew(const void* original) const override
		{
			return new MapType(*((const MapType*)original));
		}

		virtual void* MoveDuplicateNew(const void* original) const override
		{
			return new MapType(std::move(*((const MapType*)original)));
		}

	public: // Drawing
		virtual bool DrawType(TypeDrawingContext& context, std::function<void()> afterLabelCall) const override;

	public: // Serializing
		virtual void Serialize(YAML::Emitter& emitter, SerializationContext serializationContext) const override;

		virtual void Deserialize(const YAML::Node& node, SerializationContext serializationContext) const override;
	};

	template<typename KeyType, typename ElementType>
	class THashMapType : public TypeBase, public TypeDrawerBase, public ITypeSerializerBase
	{
	public:
		using MapType = THashMap<KeyType, ElementType>;
		using PairType = std::pair<KeyType, ElementType>;

		THashMapType(const char* name)
			: TypeBase(name, sizeof(MapType))
		{
			SetTypeDrawer(this);
			SetTypeSerializer(this);
		}

		const TypeBase* GetMapElementType() const { return GetTypeDescriptor<ElementType>(); }

		const TypeBase* GetMapKeyType() const { return GetTypeDescriptor<KeyType>(); }

		const TypeBase* GetMapPairType() const { return GetTypeDescriptor<PairType>(); }

		void* GetMapElementAdress(size_t id, void* arrayStart) const
		{
			MapType* array = (MapType*)arrayStart;

			auto iterator = array->begin();
			for (size_t i = 0; i < id; ++i)
				++iterator;
			return &(iterator->second);
		}
		const void* GetMapKeyAdress(size_t id, void* arrayStart) const
		{
			MapType* array = (MapType*)arrayStart;

			auto iterator = array->begin();
			for (size_t i = 0; i < id; ++i)
				++iterator;
			return &(iterator->first);
		}
		const void* GetMapPairAdress(size_t id, void* arrayStart) const
		{
			MapType* array = (MapType*)arrayStart;

			auto iterator = array->begin();
			for (size_t i = 0; i < id; ++i)
				++iterator;
			return &(*iterator);
		}

		size_t GetMapSize(void* arrayStart) const
		{
			MapType* array = (MapType*)arrayStart;
			return array->size();
		}

		virtual void* CreateNew() const override
		{
			return new MapType();
		}

		virtual void  Destroy(void* object) const override
		{
			delete ((MapType*)object);
		}
		virtual void DestroyInPlace(void* object) const override
		{
			((MapType*)object)->~Type();
		}

	protected:

		virtual void* CreateNewInPlace(void* memory) const override
		{
			if constexpr (std::is_default_constructible<ElementType>::value && std::is_abstract<ElementType>::value == false)
				return new(memory) MapType{};
			else
				return nullptr;
		}
		virtual void* DuplicateNew(const void* original) const override
		{
			return new MapType(*((const MapType*)original));
		}

		virtual void* MoveDuplicateNew(const void* original) const override
		{
			return new MapType(std::move(*((const MapType*)original)));
		}

	public: // Drawing
		virtual bool DrawType(TypeDrawingContext& context, std::function<void()> afterLabelCall) const override;

	public: // Serializing
		virtual void Serialize(YAML::Emitter& emitter, SerializationContext serializationContext) const override;

		virtual void Deserialize(const YAML::Node& node, SerializationContext serializationContext) const override;
	};


}

#include "rttiMapType.inl"
