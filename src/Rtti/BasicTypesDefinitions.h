#pragma once
#include "Type/rttiTypeBuilder.h"
#include "Class/rttiClassTraitsMacros.h"

#include "Serialization/rttiSerialization.h"

//#include "../Editor/BasicTypeDrawers.h"
#include "../Common/StringUtility.h"


RTTI_DEFINE(void, {
    RTTI_DISPLAY_NAME("Void");
    //RTTI_DRAWER(editor::VoidDrawer);
    //RTTI_SERIALIZER(rtti::TypeSerializerNoSerialize);
})

RTTI_DEFINE(char, {
    RTTI_DISPLAY_NAME("Char");
    //RTTI_DRAWER(editor::IntDrawer<int8>); // TODO char drawer?
    //RTTI_SERIALIZER(TTypeSerializerCast<char>);

    RTTI_EXPOSE_TO_VSL();
})

//////////////// Float

RTTI_DEFINE(float, {
    RTTI_DISPLAY_NAME("Float32");
    //RTTI_DRAWER(editor::FloatDrawer<float>);
    //RTTI_SERIALIZER(TTypeSerializerCast<float>);

    RTTI_EXPOSE_TO_VSL();
})

RTTI_DEFINE(double, {
    RTTI_DISPLAY_NAME("Float64");
    //RTTI_DRAWER(editor::FloatDrawer<double>);
    //RTTI_SERIALIZER(TTypeSerializerCast<double>);

    RTTI_EXPOSE_TO_VSL();
})

//////////////// Int

RTTI_DEFINE(int8, {
    RTTI_DISPLAY_NAME("Int8");
    //RTTI_DRAWER(editor::IntDrawer<int8>);
    //RTTI_SERIALIZER(TTypeSerializerCast<int8>);

    RTTI_EXPOSE_TO_VSL();
})
    
RTTI_DEFINE(int16, {
    RTTI_DISPLAY_NAME("Int16");
    //RTTI_DRAWER(editor::IntDrawer<int16>);
    //RTTI_SERIALIZER(TTypeSerializerCast<int16>);

    RTTI_EXPOSE_TO_VSL();
})

RTTI_DEFINE(int32, {
    RTTI_DISPLAY_NAME("Int32");
    //RTTI_DRAWER(editor::IntDrawer<int32>);
    //RTTI_SERIALIZER(TTypeSerializerCast<int32>);

    RTTI_EXPOSE_TO_VSL();
})

RTTI_DEFINE(int64, {
    RTTI_DISPLAY_NAME("Int64");
    //RTTI_DRAWER(editor::IntDrawer<int64>);
    //RTTI_SERIALIZER(TTypeSerializerCast<int64>);

    RTTI_EXPOSE_TO_VSL();
})

RTTI_DEFINE(uint8, {
    RTTI_DISPLAY_NAME("Unsigned Int8");
    //RTTI_DRAWER(editor::IntDrawer<uint8>);
    //RTTI_SERIALIZER(TTypeSerializerCast<uint8>);

    RTTI_EXPOSE_TO_VSL();
})
    
RTTI_DEFINE(uint16, {
    RTTI_DISPLAY_NAME("Unsigned Int16");
    //RTTI_DRAWER(editor::IntDrawer<uint16>);
    //RTTI_SERIALIZER(TTypeSerializerCast<uint16>);

    RTTI_EXPOSE_TO_VSL();
})

RTTI_DEFINE(uint32, {
    RTTI_DISPLAY_NAME("Unsigned Int32");
    //RTTI_DRAWER(editor::IntDrawer<uint32>);
    //RTTI_SERIALIZER(TTypeSerializerCast<uint32>);

    RTTI_EXPOSE_TO_VSL();
})

RTTI_DEFINE(uint64, {
    RTTI_DISPLAY_NAME("Unsigned Int64");
    //RTTI_DRAWER(editor::IntDrawer<uint64>);
    //RTTI_SERIALIZER(TTypeSerializerCast<uint64>);

    RTTI_EXPOSE_TO_VSL();
})

//////////////// Bool

RTTI_DEFINE(bool, {
    RTTI_DISPLAY_NAME("Bool");
    //RTTI_DRAWER(editor::BoolDrawer);
    //RTTI_SERIALIZER(TTypeSerializerCast<bool>);

    RTTI_EXPOSE_TO_VSL();
})

//////////////// String

RTTI_DEFINE(String, {
    RTTI_DISPLAY_NAME("String");
    //RTTI_DRAWER(editor::StringDrawer<String>);
    //RTTI_SERIALIZER(TTypeSerializerCast<String>);

    RTTI_EXPOSE_TO_VSL();
})

RTTI_DEFINE(Path, {
    RTTI_DISPLAY_NAME("Path");
    //RTTI_DRAWER(editor::PathDrawer_Save);
    //RTTI_SERIALIZER(TTypeSerializerCast<Path>);

    RTTI_EXPOSE_TO_VSL();
})

RTTI_DEFINE(Name, {
    RTTI_DISPLAY_NAME("Name");
    //RTTI_DRAWER(editor::StringDrawer<Name>);
    //RTTI_SERIALIZER(TTypeSerializerCast<Name>);

    RTTI_EXPOSE_TO_VSL();
})


//////////////// External



