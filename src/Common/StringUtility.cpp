#include "StringUtility.h"
//#include <Engine/Common/Memory/Memory.h>

// initialize to 1 as a first valid index
// 0 means uninitialized Name
Name::NameId Name::_nextFreeId{1};

/**
TempString::~TempString()
{
	Clear();
}


TempString::TempString(const char* str)
{
	_block.size = strlen(str);
	Allocate(GetSize() + 1);
	memcpy(_block.address, str, GetSize() + 1);
}

TempString::TempString(const String& str)
{
	_block.size = str.size();
	Allocate(GetSize() + 1);
	memcpy(_block.address, str.c_str(), GetSize() + 1);
}

TempString::TempString(const Name& str)
{
	_block.size = str.GetString().size();
	Allocate(GetSize() + 1);
	memcpy(_block.address, str.GetString().c_str(), GetSize() + 1);
}

TempString::TempString(TempString&& other)
	: _block(other._block)
{
	other.Invalidate();
}

TempString::TempString(const TempString& other)
{
	_block.size = other.GetSize();
	Allocate(GetSize() + 1);
	memcpy(_block.address, other.GetBuffer(), GetSize() + 1);
}

void TempString::operator=(TempString&& other)
{
	if (GetSize() != 0)
		Clear();

	_block = other._block;
	
	other.Invalidate();
}

void TempString::operator=(const TempString& other)
{
	*this = other.GetBuffer();
}

void TempString::operator=(const char* str)
{
	if (GetSize() != 0)
		Clear();

	_block.size = strlen(str);
	Allocate(_block.size + 1);
	memcpy(_block.address, str, _block.size + 1);
}

void TempString::operator=(const String& str)
{
	if (GetSize() != 0)
		Clear();

	_block.size = str.size();
	Allocate(GetSize() + 1);
	memcpy(_block.address, str.c_str(), GetSize() + 1);
}

void TempString::operator=(const Name& str)
{
	if (GetSize() != 0)
		Clear();

	_block.size = str.GetString().size();
	Allocate(GetSize() + 1);
	memcpy(_block.address, str.GetString().c_str(), GetSize() + 1);
}

bool TempString::operator==(const TempString& other) const
{
	return strcmp(GetBuffer(), other.GetBuffer()) == 0;
}

bool TempString::operator==(const char* other) const
{
	return strcmp(GetBuffer(), other) == 0;
}

bool TempString::operator==(const String& other) const
{
	return strcmp(GetBuffer(), other.c_str()) == 0;
}

bool TempString::operator==(const Name& other) const
{
	return strcmp(GetBuffer(), other.GetString().c_str()) == 0;
}

void TempString::Clear()
{
	if (_block.address != nullptr)
	{
		memory::FrameMemoryPool::Get().Free(_block);
		Invalidate();
	}
}

void TempString::Allocate(uint32 size)
{
	YK_ASSERT(GetBuffer() == nullptr);
	_block = memory::FrameMemoryPool::Get().Allocate(size);
}*/
