#pragma once

#include <iostream>
#include <dxgi1_5.h>
#include <d3d12.h>

#include <SDL.h>

#include "renderer.h"
#include "dx12_utils.h"
#include "dx12_command_list.h"

#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3d12.lib")
#pragma comment(lib, "D3DCompiler.lib")

class SDL_Window;
class ID3D12Device;
class ID3D12CommandQueue;
class ID3D12CommandAllocator;
class ID3D12GraphicsCommandList;
class ID3D12DescriptorHeap;
class ID3D12Resource;
class ID3D12Fence;

class Dx12Engine : public Renderer
{
public:
	void Init(std::span<float> geometry) override;
	void Cleanup() override;
	void Run() override;
private:
	void Draw();
	void InitializeSdl();
	void CreateDxgiAndEnumAdapters();
	void CreateDxDevice();
	void CreateCommandQueue();
	void CreateSwapChain();
	void CreateCommandAllocator();
	void CreateCommandList();
	void CreateDescriptorHeap();
	void CreateFrameResources();
	void WaitForPreviousFrame();
	void CreateFence();
	void CreateRootSignature();
	void CreatePipelineState();
	void CreateVertexBuffer(std::span<float> geometry);
	void ExecuteCommandList();
	void RecordCommandList(ID3D12GraphicsCommandList* command_list_);
	void HandleInput(SDL_Event& event);

	HWND hwnd;
	UINT rtv_descriptor_size_;
	UINT frame_index_ = { 0 };
	UINT64 fence_value_;
	HANDLE fence_event_;
	D3D12_VIEWPORT viewport_;
	D3D12_RECT scissor_rect_;
	SDL_Window* window_;
	IDXGIFactory5* dxgi_factory_;
	IDXGIAdapter1* dxgi_adapter_;
	IDXGISwapChain3* dxgi_swap_chain_;
	ID3D12Device* device_;
	ID3D12CommandQueue* command_queue_;
	ID3D12DescriptorHeap* rtv_descriptor_heap_;
	ID3D12Fence* fence_;
	ID3D12RootSignature* root_signature_;
	ID3D12PipelineState* pipeline_state_;
	TStaticArray<ID3D12Resource*, 2> render_targets_;
	ID3D12Resource* vertex_buffer_;
	D3D12_VERTEX_BUFFER_VIEW vertex_buffer_view_;
	UINT num_of_vertices_ = { 0 };
	Dx12CommandListAllocator* dx12_command_allocator_;
	Dx12CommandList* dx12_command_list_;

	bool quit;
	SDL_Event event;
};
