struct VSInput
{
	float3 position : POSITION;
	float3 color : COLOR;
};

static const float3 colors[3] = {
	float3(1.0f, 0.0f, 0.0f),
	float3(0.0f, 1.0f, 0.0f),
	float3(0.0f, 0.0f, 1.0f)
};

struct VSOutput
{
	float4 position : SV_POSITION;
	float3 color : COLOR;
};

VSOutput VSMain(VSInput input)
{
	VSOutput vsOut;
	vsOut.position = float4(input.position, 1.0f);
	vsOut.color = colors[1];
	return vsOut;
}
float4 PSMain(VSOutput psInput) : SV_TARGET
{
	return float4(psInput.color.rgb, 1.0f);
}