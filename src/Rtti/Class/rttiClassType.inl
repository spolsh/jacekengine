#pragma once
#include "rttiClassType.h"
#include "rttiClassBuilder.h"

namespace rtti
{
	template<typename Ty>
	inline bool ClassType::IsA() const
	{
		TypeBase* type = GetTypeDescriptor<Ty>();
		ClassType* c = dynamic_cast<ClassType*>(type);
		return c ? IsA(c) : IsA((InterfaceType*)type);
	}

}
