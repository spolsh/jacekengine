#pragma once
#include "rttiTypeBase.h"

namespace rtti
{

	class TypeLibrary
	{
	public:

		void AddType(TypeBase* t)
		{
			_types.push_back(t);
		}

		template<typename Ty>
		void AddType()
		{
			_types.push_back(rtti::GetTypeDescriptor<Ty>());
		}

		auto begin() { return _types.begin(); }
		auto end() { return _types.end(); }
		auto cbegin() const { return _types.begin(); }
		auto cend() const { return _types.end(); }
		auto begin() const { return _types.begin(); }
		auto end() const { return _types.end(); }


	private:
		TDynArray<TypeBase*> _types;
	};

}