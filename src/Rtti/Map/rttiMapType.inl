#pragma once
#include "rttiMapType.h"
//#include <Engine/Editor/BasicTypeDrawers.h>

namespace rtti
{

	///////////////// TreeMap

	template<typename KeyType, typename ElementType>
	inline bool TTreeMapType<KeyType, ElementType>::DrawType(TypeDrawingContext& context, std::function<void()> afterLabelCall) const
	{
#ifdef RTTI_ENABLE_DRAWING
		/**/
		ImGuiTreeNodeFlags flags = ImGuiTreeNodeFlags_OpenOnArrow
			//| ImGuiTreeNodeFlags_AllowItemOverlap
			| ImGuiTreeNodeFlags_DefaultOpen
			;

		float offset = ImGui::GetCursorPosX() + context.offset * editor::LABEL_OFFSET_STEP;
		ImGui::SetCursorPosX(ImGui::GetCursorPosX() + context.offset * editor::LABEL_OFFSET_STEP);
		ImGui::SetNextItemWidth(10);
		bool opened = ImGui::TreeNodeEx(IMGUI_ID(context.property->GetDisplayName()), flags);

		afterLabelCall();

		ImGui::SameLine(offset + 50, 50);
		ImGui::SetNextItemWidth(200);
		int size = (int)GetMapSize(context.objectStart);
		//bool resized = ImGui::InputInt(IMGUI_ID("size"), &size);
		std::stringstream stream;
		stream << "size " << size;
		ImGui::Text(stream.str().c_str());

		if (opened)
		{
			MapType* map = (MapType*)context.objectStart;
			for (auto _it : *map)
			{
				//rtti::Draw(_it);
			}

			ImGui::TreePop();
		}
		/**/
#endif // RTTI_ENABLE_DRAWING
		return false;
	}
	
	template<typename KeyType, typename ElementType>
	inline void TTreeMapType<KeyType, ElementType>::Serialize(YAML::Emitter& emitter, SerializationContext serializationContext) const
	{
#ifdef RTTI_ENABLE_SERIALIZATION
		emitter << YAML::Value << YAML::BeginSeq;

		size_t arraySize = GetMapSize(serializationContext.objectStart);

		for (size_t i = 0; i < arraySize; ++i)
		{
			SerializationContext elementContext{ serializationContext };
			elementContext.objectStart = (void*)GetMapPairAdress(i, serializationContext.objectStart);

			GetMapPairType()->GetTypeSerializer()->Serialize(emitter, elementContext);
		}

		emitter << YAML::EndSeq;
#endif
	}

	template<typename KeyType, typename ElementType>
	inline void TTreeMapType<KeyType, ElementType>::Deserialize(const YAML::Node& node, SerializationContext serializationContext) const
	{
#ifdef RTTI_ENABLE_SERIALIZATION
		YK_ASSERT(node);
		YK_ASSERT(node.IsSequence());
		
		MapType* map = (MapType*)serializationContext.objectStart;
		
		map->clear();
		
		for (auto _it = node.begin(); _it != node.end(); ++_it)
		{
			const YAML::Node& nodeSampler = *_it;
			RE_ASSERT(nodeSampler);
			RE_ASSERT(nodeSampler.IsMap());


			PairType pair;

			SerializationContext elementContext{ serializationContext };
			elementContext.objectStart = &pair;

			GetMapPairType()->GetTypeSerializer()->Deserialize(nodeSampler, elementContext);

			map->insert(pair);
		}
#endif
	}

	///////////////// HashMap

	template<typename KeyType, typename ElementType>
	inline bool THashMapType<KeyType, ElementType>::DrawType(TypeDrawingContext& context, std::function<void()> afterLabelCall) const
	{
#ifdef RTTI_ENABLE_DRAWING
		/**/
		ImGuiTreeNodeFlags flags = ImGuiTreeNodeFlags_OpenOnArrow
			//| ImGuiTreeNodeFlags_AllowItemOverlap
			| ImGuiTreeNodeFlags_DefaultOpen
			;

		float offset = ImGui::GetCursorPosX() + context.offset * editor::LABEL_OFFSET_STEP;
		ImGui::SetCursorPosX(ImGui::GetCursorPosX() + context.offset * editor::LABEL_OFFSET_STEP);
		ImGui::SetNextItemWidth(10);
		bool opened = ImGui::TreeNodeEx(IMGUI_ID(context.property->GetDisplayName()), flags);

		afterLabelCall();

		ImGui::SameLine(offset + 50, 50);
		ImGui::SetNextItemWidth(200);
		int size = (int)GetMapSize(context.objectStart);
		//bool resized = ImGui::InputInt(IMGUI_ID("size"), &size);
		std::stringstream stream;
		stream << "size " << size;
		ImGui::Text(stream.str().c_str());

		if (opened)
		{
			MapType* map = (MapType*)context.objectStart;
			for (auto _it : *map)
			{
				//rtti::Draw(_it);
			}

			ImGui::TreePop();
		}
#endif
		/**/
		return false;
	}

	template<typename KeyType, typename ElementType>
	inline void THashMapType<KeyType, ElementType>::Serialize(YAML::Emitter& emitter, SerializationContext serializationContext) const
	{
#ifdef RTTI_ENABLE_SERIALIZATION
		emitter << YAML::Value << YAML::BeginSeq;

		size_t arraySize = GetMapSize(serializationContext.objectStart);

		for (size_t i = 0; i < arraySize; ++i)
		{
			SerializationContext elementContext{ serializationContext };
			elementContext.objectStart = (void*)GetMapPairAdress(i, serializationContext.objectStart);

			GetMapPairType()->GetTypeSerializer()->Serialize(emitter, elementContext);
		}

		emitter << YAML::EndSeq;
#endif
	}

	template<typename KeyType, typename ElementType>
	inline void THashMapType<KeyType, ElementType>::Deserialize(const YAML::Node& node, SerializationContext serializationContext) const
	{
#ifdef RTTI_ENABLE_SERIALIZATION
		RE_ASSERT(node);
		RE_ASSERT(node.IsSequence());

		MapType* map = (MapType*)serializationContext.objectStart;

		map->clear();

		for (auto _it = node.begin(); _it != node.end(); ++_it)
		{
			const YAML::Node& nodeSampler = *_it;
			RE_ASSERT(nodeSampler);
			RE_ASSERT(nodeSampler.IsMap());


			PairType pair;

			SerializationContext elementContext{ serializationContext };
			elementContext.objectStart = &pair;

			GetMapPairType()->GetTypeSerializer()->Deserialize(nodeSampler, elementContext);

			map->insert(pair);
		}
#endif
	}

}