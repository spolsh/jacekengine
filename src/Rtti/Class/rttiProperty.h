#pragma once
#include "../Drawing/rttiTypeDrawer.h"
#include "../Function/rttiFunction.h"
#include "../Serialization/rttiSerialization.h"

namespace rtti
{
	class TypeBase;
	class ClassType;

	// TODO
	// Property made of 2 functions
	// override set || get function

	class Property : public TypeDrawerBase, public ITypeSerializerBase
	{
	public:
		using DrawingConditionType = std::function<bool()>;
	public:
		Property(TypeBase* type, const char* nativeName, const ClassType* owningType, uint32 offset, bool isReadOnly = false, bool isReference = false);
		~Property();

		/// /brief sets native name of the varriable
		Property& SetNativeName(const char* name) { _nativeName = name; return *this; }

		/// /brief returns native name of the varriable
		NO_DISCARD const char* GetNativeName() const { return _nativeName; }
		
		/// /brief sets display name of the varriable in editor
		Property& SetDisplayName(const char* name) { _displayName = name; return *this; }

		/// /brief returns display name of the varriable in editor
		NO_DISCARD const char* GetDisplayName() const { return _displayName; }

		/// /brief sets category the property lies in in the editor
		Property& SetPropertyCategory(const char* category) { TypeDrawerBase::SetPropertyCategory(category); return *this; }
		
		/// GetCategory inherited from TypeDrawerBase

		/// TODO
		Property& SetDrawingCondition(DrawingConditionType& drawingCondition) { _drawingCondition = drawingCondition; return *this; }

		/// /brief returns tooltip for the property
		NO_DISCARD const char* GetTooltip() const { return _tooltip; }

		/// /brief sets up tooltip for the property
		Property& SetTooltip(const char* tooltip) { _tooltip = tooltip; return *this; }

		// /brief changes drawer mode to read only
		Property& SetReadOnly(bool isReadOnly) { _readOnly = isReadOnly; return *this; }

		void SetIsReadOnly(bool isReadonly) { _readOnly = isReadonly; }

		// /brief returns if drawer mode is read only
		NO_DISCARD bool IsReadOnly() const { return _readOnly; }

		void SetIsReference(bool isReference) { _isReference = isReference; }

		// /brief returns if drawer mode is read only
		NO_DISCARD bool IsReference() const { return _isReference; }

		/// /brief returns type of property
		NO_DISCARD TypeBase* GetPropertyType() const { return _propertyType; }
		
		void SetPropertyType(TypeBase* type) { _propertyType = type; }

		/// /brief returns type of a class that uses the property
		///
		/// should be always non null, otherwise will assert
		NO_DISCARD const ClassType* GetOwningClass() const { YK_ASSERT(_owningClass); return _owningClass; }

		/// /brief given adress of the owning class returns address of the property 
		NO_DISCARD void* GetPropertyAddres(void* classStartAdress) const
		{
			return ((char*)classStartAdress) + _offset;
		}

		/// /brief given property address returns address of an owner
		NO_DISCARD void* GetOwnerAddres(void* propertyAdress) const
		{
			return ((char*)propertyAdress) - _offset;
		}

		NO_DISCARD bool operator == (const Property& other) const;

		void SetOffset(uint32 offset) { _offset = offset; }
		NO_DISCARD uint32 GetOffset() const { return _offset;  }

		virtual bool DrawHeader(TypeDrawingContext& context) const override;
		virtual void EndHeader(TypeDrawingContext& context) const override;

		virtual bool DrawType(TypeDrawingContext& context) const override;

	public:
		virtual void Serialize(YAML::Emitter& emitter, SerializationContext serializationContext) const override;
		virtual void Deserialize(const YAML::Node& node, SerializationContext serializationContext) const override;

		
		/// /brief sets override type drawer for the property
		Property& SetOverrideTypeDrawer(TypeDrawerBase* typeDrawer) { _isTypeDrawerAReference = true; _overrideTypeDrawer = typeDrawer; return *this; }
		
		/// /brief	sets up inspector type drawer override to a newly created type drawer
		/// 
		/// @DrawerType - type of a drawer to be created
		template<typename DrawerType, typename ... Args>
		Property& SetOverrideTypeDrawer(Args... args)
		{
			if (_overrideTypeDrawer && !_isTypeDrawerAReference)
				delete _overrideTypeDrawer;

			_overrideTypeDrawer = new DrawerType(args...);
			_isTypeDrawerAReference = false;

			return *this;
		}

		/// /brief returns override type drawer for the property or null if type defined one should be used
		NO_DISCARD TypeDrawerBase* GetOverrideTypeDrawer() const { return _overrideTypeDrawer; }


		/// /brief sets override type serializer for the property
		Property& SetOverrideTypeSerializer(ITypeSerializerBase* typeSerializer) { _isTypeSerializerAReference = true; _overrideTypeSerializer = typeSerializer; return *this; }
		
		/// /brief sets up type serializer
		///
		/// Type serializer is used to define how to serialize the type 
		/// @SerializerType - type of a serializer to be created
		template<typename SerializerType, typename ... Args>
		Property& SetOverrideTypeSerializer(Args... args)
		{
			if (_overrideTypeSerializer && !_isTypeSerializerAReference)
				delete _overrideTypeSerializer;

			_overrideTypeSerializer = new SerializerType(args...);
			_isTypeSerializerAReference = false;

			return *this;
		}

		/// /brief returns override type serializer for the property or null if type defined one should be used
		NO_DISCARD ITypeSerializerBase* GetOverrideTypeSerializer() const { return _overrideTypeSerializer; }

		/// /brief	sets up an override setter for the type varriables
		/// 
		/// Setter function is a function that is called when varriable changes inside editor
		/// If there is no setter varriable is simply set
		/// TODO make possible doing "fake" varriable, created only from setter and getter but not from real varriable
		Property& SetOverridePropertySetter(const FunctionTypeBase* s) { _overrideSetter = s; return *this; }
		
		/// /brief	returns an override setter for the type varriables
		/// 
		/// Setter function is a function that is called when varriable changes inside editor
		/// If there is no setter varriable is simply set
		/// property setter overrides default type behaviour if present
		/// TODO make possible doing "fake" varriable, created only from setter and getter but not from real varriable
		NO_DISCARD const FunctionTypeBase* GetOverridePropertySetter() const { return _overrideSetter;  }

		/// /brief	sets up an override getter for the type varriables
		/// 
		/// Setter function is a function that is called when varriable changes inside editor
		/// If there is no setter varriable is simply set
		/// property getter overrides default type behaviour if present
		/// TODO make possible doing "fake" varriable, created only from setter and getter but not from real varriable
		Property& SetOverridePropertyGetter(const FunctionTypeBase* s) { _overrideGetter = s; return *this; }

		/// /brief	returns an override getter for the type varriables
		/// 
		/// Setter function is a function that is called when varriable changes inside editor
		/// If there is no setter varriable is simply set
		/// property getter overrides default type behaviour if present
		/// TODO make possible doing "fake" varriable, created only from setter and getter but not from real varriable
		NO_DISCARD const FunctionTypeBase* GetOverridePropertyGetter() const { return _overrideGetter; }


		Property& SetOverridePropertyCondition(const FunctionTypeBase* s) { _overrideCondition = s; return *this; }
		
		NO_DISCARD const FunctionTypeBase* GetOverridePropertyCondition() const { return _overrideCondition; }

	private:
		// TODO we need a mechanism of passing right object to the condition
		// we could do so by void* , but then the user will have to cast _it manually, what is not very handy
		DrawingConditionType	_drawingCondition;

		TypeBase*				_propertyType;				
		const ClassType*		_owningClass;				

		TypeDrawerBase*			_overrideTypeDrawer;
		ITypeSerializerBase*	_overrideTypeSerializer;

		const FunctionTypeBase const*		_overrideGetter;
		const FunctionTypeBase const*		_overrideSetter;
		const FunctionTypeBase const*		_overrideCondition;

		// name of property figuring in the code
		const char*				_nativeName;			

		// name of property shown in gui
		const char*				_displayName;			
		
		// description of property function
		const char*				_tooltip;

		// offset of property from the beginning of the object
		uint32					_offset;				

		// could this property be modified in the ui?
		uint8					_readOnly : 1;

		// should be treated as a reference? 
		// when speaking about function parameters reference is treated as a return value
		uint8					_isReference : 1;

		// flag to mark if serializer should be deleted
		uint8					_isTypeSerializerAReference : 1;
		// flag to mark if drawer should be deleted
		uint8					_isTypeDrawerAReference : 1;
	};
}
