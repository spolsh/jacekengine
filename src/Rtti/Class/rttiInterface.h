#pragma once
#include "../Function/rttiFunction.h"

///////////// WIP
// its only a prototype of potential support for interfaces
// 
// The idea is to dissallow multiple inheritance while using rtti 
// right now it's dangerous to even use interfaces outside of root class in the hierarchy
// adding a new, even possibly empty interface adds a new varriable in front of class ( at least in case of visual studio)
//		which invalidates property offsets
// supporting interfaces would require fixing up all the property pointers from base class
// 
// Interface is meant to support virtual-like functions with possibly a default implementation 
//		( if it doesn't involve storing any parameters in interface )
// adding any varriables to interface is forbidden - it will invalidate class Properties in unpredictable way
//
// virtual-like functions mean possibility for overriding
// 
// should contain native/display name, tooltip, list of all classes implementing it
// 
// not clear if Interface should be a TypeBase or it's own thing
//
// when either implementing interface or inheriting from a class that did
//	we should have an ability to override all the functions with the same name as ones declared in the interface
//
// if function will not be found searching for rtti ptr should return nullptr
// if while adding function with the same name already exists it should be overriden
//

namespace rtti
{
	class ClassType;

	class InterfaceType : public TypeBase
	{
	public:
		InterfaceType(const char* name, uint16 size);

		/// /brief returns iterator of directly deriving classes from this one
		///
		/// usefull for iterating over class types
		NO_DISCARD TypeIterator<TDynArray<ClassType*>> GetDirectlyDerivedIterator() const
		{
			return TypeIterator<TDynArray<ClassType*>>(_derived);
		}

		/// /brief returns iterator of (directly or not) deriving classes from this one
		///
		/// usefull for iterating over class types
		NO_DISCARD TypeIterator<TDynArray<ClassType*>> GetAllDerivedIterator() const
		{
			return TypeIterator<TDynArray<ClassType*>>(_derivedAll);
		}

		/// /brief searches over all derived types one with a given native name
		NO_DISCARD ClassType* FindSubclassWithNativeName(const char* nativeName);

		/// /brief searches over all derived types one with a given display name
		NO_DISCARD ClassType* FindSubclassWithDisplayName(const char* displayName);

		/// /brief searches over directly derived types one with a given native name
		NO_DISCARD ClassType* FindDirectSubclassWithNativeName(const char* nativeName);

		/// /brief searches over directly derived types one with a given display name
		NO_DISCARD ClassType* FindDirectSubclassWithDisplayName(const char* displayName);

	public: // function

		/// /brief declares a new function to the class
		/// 
		/// @returns added function value to allow chained setup _it's parameters
		/// @see RTTI_MEMBER_FUNCTION, _it is adviced to use the function instead of directly calling
		FunctionTypeBase* AddMemberFunction(FunctionTypeBase* function)
		{
			_memberFunctions.push_back(std::shared_ptr<FunctionTypeBase>(function));
			return function;
		}

		/// /brief returns id'th function of the class
		/// 
		/// @returns nullptr if id isn't in function number range
		NO_DISCARD FunctionTypeBase* GetMemberFunction(size_t id) const
		{
			if (id >= _memberFunctions.size())
			{
				return nullptr;
			}
			return _memberFunctions[id].get();
		}

		/// /brief returns function with @nativeName
		/// 
		/// @returns nullptr if there is no such a function
		NO_DISCARD FunctionTypeBase* FindMemberFunctionByNativeName(const char* nativeName)
		{
			for (int i = 0; i < _memberFunctions.size(); ++i)
			{
				if (strcmp(nativeName, _memberFunctions[i]->GetNativeName()) == 0)
				{
					return _memberFunctions[i].get();
				}
			}
			return nullptr;
		}

		/// /brief returns function with @displayName
		/// 
		/// @returns nullptr if there is no such a function
		NO_DISCARD FunctionTypeBase* FindMemberFunctionByDisplayName(const char* displayName)
		{
			for (int i = 0; i < _memberFunctions.size(); ++i)
			{
				if (strcmp(displayName, _memberFunctions[i]->GetDisplayName()) == 0)
				{
					return _memberFunctions[i].get();
				}
			}
			return nullptr;
		}

		/// /brief returns how many functions are declared to the class
		NO_DISCARD size_t GetFunctionCount() const { return _memberFunctions.size(); }

	protected:

		void AddDerived(ClassType* derived)
		{
			if (std::find(_derived.begin(), _derived.end(), derived) != _derived.end())
				return; // already added;

			_derived.push_back(derived);
			AddToAllDerrived(derived);
		}
		void AddToAllDerrived(ClassType* derived)
		{
			if (std::find(_derivedAll.begin(), _derivedAll.end(), derived) != _derivedAll.end())
				return; // already added;
			_derivedAll.push_back(derived);
		}

	private:
		TDynArray<ClassType*> _derived;
		TDynArray<ClassType*> _derivedAll;
		TDynArray<std::shared_ptr<FunctionTypeBase>> _memberFunctions;

		friend class ClassType;
	};

	template<typename Type>
	class TInterface : public InterfaceType
	{
	public:
		TInterface(const char* name)
			: InterfaceType(name, sizeof(Type))
		{
			static_assert(std::is_class<Type>::value
				, SASSERT_ERROR_MESSAGE("TClassType", "Interface has to be a class")
				);
			static_assert(std::is_polymorphic<Type>::value
				, SASSERT_ERROR_MESSAGE("TClassType", "Interface has to have virtual functions")
				);
		}

		virtual void* CreateNewInPlace(void* memory) const override
		{
			return nullptr;
		}
		virtual void* CreateNew() const override
		{
			return nullptr;
		}

		virtual void Destroy(void* object) const override
		{
		}

		virtual void DestroyInPlace(void* object) const override
		{
		}

	protected:
		virtual void* DuplicateNew(const void* original) const override
		{
			return nullptr;
		}

		virtual void* MoveDuplicateNew(const void* original) const override
		{
			return nullptr;
		}
	};

	/// /brief accessor of class types
	template<typename Type>
		requires(std::is_class_v<Type>)
	inline InterfaceType* GetInterfaceDescriptor()
	{
		RE_ASSERT((dynamic_cast<InterfaceType*>(GetTypeDescriptor<Type>()) != nullptr));
		return (InterfaceType*)GetTypeDescriptor<Type>();
	}

}