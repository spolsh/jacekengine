#pragma once
#include "../RttiCore.h"

namespace rtti
{
	class TypeDrawerBase;
	class FunctionTypeBase;
	class SerializationContext;
	using ITypeSerializerBase = TISerializerBase<rtti::SerializationContext>;

	class TypeBase
	{
	public:
		TypeBase(const char* name, uint16 size);
		~TypeBase();

		static TDynArray<TypeBase*>& GetAllTypes()
		{
			static TDynArray<TypeBase*> allTypes;
			return allTypes;
		}

		/// /brief	any final operations that have to be applied after type is configured
		virtual void FinalizeInitialization();

		/// /brief returns native name of the type
		///
		/// Native name is the name of type with inclusion of any namespaces
		const char* GetNativeName() const { return _nativeName.c_str(); }

		/// /brief changes display name of the type
		///
		/// Native name is the name of type with inclusion of any namespaces
		void SetNativeName(const char* nativeName) { _nativeName = nativeName; }

		/// /brief returns display name of the type
		///
		/// Display name is a name that will be displayed in editor
		const char* GetDisplayName() const { return _displayName.c_str(); }
		
		/// /brief changes display name of the type
		///
		/// Display name is a name that will be displayed in editor
		void SetDisplayName(const char* displayName) { _displayName = displayName; }


		/// /brief returns tooltip for the type
		const char* GetTooltip() const { return _tooltip.c_str(); }
		
		/// /brief sets up tooltip for the type
		void SetTooltip(const char* tooltip) { _tooltip = tooltip; }

		/// /brief returns size of the type (aka result of sizeof operator)
		size_t GetTypeSize() const { return _size; }

		/// /brief creates a new object of that type
		virtual void* CreateNew() const = 0;

		/// /brief creates a new object of that type in a given adress
		virtual void* CreateNewInPlace(void * memory) const = 0;

		/// /brief changes value of ptr to the value of valueString
		///
		/// returns if conversion was successful
		virtual bool SetFromString(void* ptr, const char* valueString) const { return false; };

		/// /brief calls proper destructor and delete on an object of that type
		virtual void  Destroy(void* object) const = 0;

		/// /brief calls proper destructor of object
		virtual void DestroyInPlace(void* object) const = 0;

		/// /brief duplicates the type using a copy constructor
		virtual void* DuplicateNew(const void* original) const = 0;

		/// /brief duplicates the type using a move constructor
		virtual void* MoveDuplicateNew(const void* original) const = 0;

		/// /brief	returns inspector type drawer 
		/// 
		/// TODO what about constify the result?
		TypeDrawerBase* GetTypeDrawer() const { return _typeDrawer; }
		
		/// /brief	sets up inspector type drawer to a provided reference
		/// 
		/// TODO what about constify @typeDrawer ?
		void SetTypeDrawer(TypeDrawerBase* typeDrawer) { _typeDrawer = typeDrawer; _isTypeDrawerAReference = true; }

		/// /brief	sets up inspector type drawer to a newly created type drawer
		/// 
		/// @DrawerType - type of a drawer to be created
		template<typename DrawerType, typename ... Args>
		void SetTypeDrawer(Args... args) 
		{ 
			if (_typeDrawer && !_isTypeDrawerAReference)
				delete _typeDrawer;

			_typeDrawer = new DrawerType(args...); 
			_isTypeDrawerAReference = false; 
		}

		/// /brief	returns a setter for the type varriables
		/// 
		/// Setter function is a function that is called when varriable changes inside editor
		/// If there is no setter varriable is simply set
		/// TODO make possible doing "fake" varriable, created only from setter and getter but not from real varriable
		NO_DISCARD FunctionTypeBase* GetDefaultSetter() const { return _defaultSetter; }

		/// /brief	sets up a setter for the type varriables
		/// 
		/// Setter function is a function that is called when varriable changes inside editor
		/// If there is no setter varriable is simply set
		/// TODO make possible doing "fake" varriable, created only from setter and getter but not from real varriable
		void SetDefaultSetter(FunctionTypeBase* setter) { _defaultSetter = setter; }

		/// /brief	returns a getter for the type varriables
		/// 
		/// Getter function is a function that is called to obtain varriable value inside editor
		/// If there is no getter varriable is simply returned
		/// TODO make possible doing "fake" varriable, created only from setter and getter but not from real varriable
		NO_DISCARD FunctionTypeBase* GetDefaultGetter() const { return _defaultGetter; }

		/// /brief	returns a getter for the type varriables
		/// 
		/// Condition function is a function that returns if the varriable can be displayed in the editor
		void SetDefaultGetter(FunctionTypeBase* getter) { _defaultGetter = getter; }

		/// /brief	returns a condition for the type varriables
		/// 
		/// Condition function is a function that returns if the varriable can be displayed in the editor
		NO_DISCARD FunctionTypeBase* GetDefaultCondition() const { return _defaultCondition; }

		/// /brief	returns a codition for the type varriables
		/// 
		/// Getter function is a function that is called to obtain varriable value inside editor
		/// If there is no getter varriable is simply returned
		/// TODO make possible doing "fake" varriable, created only from setter and getter but not from real varriable
		void SetDefaultCondition(FunctionTypeBase* getter) 
		{ 
			_defaultCondition = getter;
		}

		void SetTypeCategory(const char* category) { _typeCategory = category; }

		NO_DISCARD const char* GetTypeCategory() const { return _typeCategory.c_str(); }

		NO_DISCARD bool IsExposedToVSL() const { return _exposedToVSL; }

		void ExposeToVSL(bool b = true) { _exposedToVSL = b; }
		
	public: // Serialization

		/// /brief sets up type serializer
		///
		/// Type serializer is used to define how to serialize the type 
		void SetTypeSerializer(ITypeSerializerBase* typeSerializer) { _typeSerializer = typeSerializer; _isTypeSerializerAReference = true; }
		
		/// /brief sets up type serializer
		///
		/// Type serializer is used to define how to serialize the type 
		/// @SerializerType - type of a serializer to be created
		template<typename SerializerType, typename ... Args>
		void SetTypeSerializer(Args... args)
		{
			if (_typeSerializer && !_isTypeSerializerAReference)
				delete _typeSerializer;

			_typeSerializer = new SerializerType(args...);
			_isTypeSerializerAReference = false;
		}

		/// /brief returns type serializer
		///
		/// Type serializer is used to define how to serialize the type 
		ITypeSerializerBase* GetTypeSerializer() const { return _typeSerializer; }


		void Serialize(YAML::Emitter& emitter, SerializationContext context) const;

		void Deserialize(const YAML::Node& node, SerializationContext context) const;

		/// /brief serializes varriable from context to the given file
		///
		/// End user should not need to use this function, use rtti::Draw function instead
		bool SerializeToFile(const Path& filePath, const class SerializationContext& context) const;
		
		/// /brief deserializes varriable from context to the given file
		///
		/// End user should not need to use this function, use rtti::Draw function instead
		bool DeserializeFromFile(const Path& filePath, const class SerializationContext& context) const;

		/// /brief serializes varriable to context to the given string
		///
		/// End user should not need to use this function, use rtti::Draw function instead
		bool SerializeToString(String& str, const class SerializationContext& context) const;
		
		/// /brief deserializes varriable from context to the given file
		///
		/// End user should not need to use this function, use rtti::Draw function instead
		bool DeserializeFromString(const std::string_view& str, const class SerializationContext& context) const;

	protected:
		String _displayName;
		String _nativeName;
		String _tooltip;
		String _typeCategory;

		uint16		_size;

		TypeDrawerBase* _typeDrawer;
		ITypeSerializerBase* _typeSerializer;
		FunctionTypeBase* _defaultGetter;
		FunctionTypeBase* _defaultSetter;
		FunctionTypeBase* _defaultCondition;

		bool _isTypeDrawerAReference : 1;
		bool _isTypeSerializerAReference : 1;
		bool _isAReference : 1;
		bool _exposedToVSL : 1;
	};

	namespace Internal
	{
		/// /brief main accessor of type descriptor data
		///
		/// you can treat this struct mostly as a function
		/// GetTypeDescriptor is implemented as a struct to support default nested types (as for DynArray)
		template<typename Type>
		struct TypeDescriptor
		{
			TypeBase* operator()()
			{
				return Type::GetStaticType();
			}
		};
	}

	

	/// /brief accessor of asset types
	template<typename Type>
	inline TypeBase* GetTypeDescriptor()
	{
		return Internal::TypeDescriptor<Type>()();
	}

	template<typename Type>
	inline bool IsA(TypeBase* type)
	{
		return type == GetTypeDescriptor<Type>();
	}
}
