#pragma once
#include <map>
#include <unordered_map>

// TODO register maps in rtti system
// TODO build our own mapto remove std counterparts

template<typename Key, typename Value>
using THashMap = std::unordered_map<Key, Value>;

template<typename Key, typename Value>
using TTreeMap = std::map<Key, Value>;

template<typename Key, typename Value>
using MultiHashMap = std::unordered_multimap<Key, Value>;

template<typename Key, typename Value>
using MultiTreeMap = std::multimap<Key, Value>;