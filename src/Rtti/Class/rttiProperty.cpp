#include "rttiProperty.h"
#include "../Type/rttiTypeBase.h"
#include "rttiClassType.h"

//#include <Engine/Editor/BasicTypeDrawers.h>


namespace rtti
{
	Property::Property(TypeBase* type, const char* nativeName, const ClassType* owningType, uint32 offset, bool isReadOnly, bool isReference)
		: _propertyType(type)
		, _owningClass(owningType)
		, _nativeName(nativeName)
		, _displayName(nativeName)
		, _offset(offset)
		, _readOnly(isReadOnly)
		, _overrideTypeDrawer(nullptr)
		, _overrideTypeSerializer(nullptr)
		, _tooltip("")
		, _overrideSetter(nullptr)
		, _overrideGetter(nullptr)
		, _isTypeDrawerAReference(false)
		, _isTypeSerializerAReference(false)
		, _isReference(isReference)
	{
	}
	Property::~Property()
	{
		if (_overrideTypeDrawer && !_isTypeDrawerAReference)
			delete _overrideTypeDrawer;

		if (_overrideTypeSerializer && !_isTypeSerializerAReference)
			delete _overrideTypeSerializer;
	}
	bool Property::operator==(const Property& other) const
	{
		return strcmp(other.GetNativeName(), GetNativeName())
			&& _owningClass->IsA(other._owningClass);
	}
	bool Property::DrawHeader(TypeDrawingContext& context) const
	{

#ifdef RTTI_ENABLE_DRAWING
		void* obj = context.objectStart;
		context.property = this;
		context.fieldName = GetDisplayName();	
		context.objectStart = GetPropertyAddres(context.objectStart);
		context.type = GetPropertyType();

		auto drawer = _overrideTypeDrawer ? _overrideTypeDrawer : GetPropertyType()->GetTypeDrawer();
		bool opened = drawer->DrawHeader(context);
		if (GetTooltip() != "" && ImGui::IsItemHovered())
		{
			ImGui::BeginTooltip();
			ImGui::Text(GetTooltip());
			ImGui::EndTooltip();
		}

		context.objectStart = obj;
#else
        bool opened = false;
#endif // RTTI_ENABLE_DRAWING

		return opened;
	}

	void Property::EndHeader(TypeDrawingContext& context) const
	{
		auto drawer = _overrideTypeDrawer ? _overrideTypeDrawer : GetPropertyType()->GetTypeDrawer();
		drawer->EndHeader(context);
	}
	bool Property::DrawType(TypeDrawingContext& context) const
	{
		TypeDrawingContext innerContext{ context };
		innerContext.property = this;
		innerContext.fieldName = GetDisplayName();
		innerContext.objectStart = GetPropertyAddres(context.objectStart);
		innerContext.type = GetPropertyType();
		innerContext.readOnly = IsReadOnly();

		auto drawer = _overrideTypeDrawer ? _overrideTypeDrawer : GetPropertyType()->GetTypeDrawer();

		YK_ASSERT(drawer);
		return drawer->DrawValue(innerContext);
	}
	void Property::Serialize(YAML::Emitter& emitter, SerializationContext context) const
	{
#ifdef RTTI_ENABLE_SERIALIZATION

		emitter << YAML::Key << _nativeName;
		
		context.objectStart = GetPropertyAddres(context.objectStart);
		context.property = this;

		auto serializer = _overrideTypeSerializer ? _overrideTypeSerializer : GetPropertyType()->GetTypeSerializer();

		YK_CHECK(serializer)->Serialize(emitter, context);
#endif // RTTI_ENABLE_SERIALIZATION
	}
	void Property::Deserialize(const YAML::Node& node, SerializationContext context) const
	{
#ifdef RTTI_ENABLE_SERIALIZATION
		auto n = node[_nativeName];
		if (n)
		{
			context.objectStart = GetPropertyAddres(context.objectStart);
			context.property = this;

			auto serializer = _overrideTypeSerializer ? _overrideTypeSerializer : GetPropertyType()->GetTypeSerializer();
			RE_CHECK(serializer)->Deserialize(n, context);
		}
		else
		{
			std::cerr << "File is ill formed\n";
		}
#endif // RTTI_ENABLE_SERIALIZATION
	}
}
