#include "rttiTypeDrawer.h"

#include "../Class/rttiProperty.h"

//#include <Engine/Editor/BasicTypeDrawers.h>

namespace rtti
{

	NO_DISCARD const rtti::FunctionTypeBase* TypeDrawingContext::GetPropertyGetter() const
	{
		return property && property->GetOverridePropertyGetter()
			? property->GetOverridePropertyGetter()
			: type->GetDefaultGetter();
	}

	NO_DISCARD const rtti::FunctionTypeBase* TypeDrawingContext::GetPropertySetter() const
	{
		return property && property->GetOverridePropertySetter()
			? property->GetOverridePropertySetter()
			: type->GetDefaultSetter();
	}

	NO_DISCARD const rtti::FunctionTypeBase* TypeDrawingContext::GetPropertyCondition() const
	{
		return property && property->GetOverridePropertyCondition()
			? property->GetOverridePropertyCondition()
			: type->GetDefaultCondition();
	}

	NO_DISCARD void* TypeDrawerBase::AllocateTempVarriable(TypeDrawingContext& context) const
	{
#ifdef RTTI_ENABLE_DRAWING
		//return context.objectStart;
		YK_ASSERT(context.type);
		size_t typeSize = context.type->GetTypeSize() * 8;
		void* tempVarriable = memory::FrameMemoryPool::Get().Allocate(typeSize).address;
		context.type->CreateNewInPlace(tempVarriable);

		auto getter = context.GetPropertyGetter();

		if (getter)
		{
			FunctionCallingContext functionContext;
			functionContext.result = tempVarriable;

			// TODO shouldnt it be an owning object?
			functionContext.object = context.property->GetOwnerAddres(context.objectStart);
			getter->Invoke(functionContext);
		}
		else
		{
			memcpy(tempVarriable, context.objectStart, (uint32)typeSize);
		}

		return tempVarriable;
#else
        return nullptr;
#endif // RTTI_ENABLE_DRAWING
	}

	void TypeDrawerBase::FreeTempVarriable(TypeDrawingContext& context, void* tempVarriable, bool changed) const
	{
#ifdef RTTI_ENABLE_DRAWING
		//return;
		size_t typeSize = context.type->GetTypeSize() * 8;
		if (changed)
		{
			if (context.readOnly == false)
			{
				auto setter = context.GetPropertySetter();
				if (setter)
				{
					FunctionCallingContext functionContext;
					functionContext.argumentPointers.push_back(tempVarriable);

					// TODO shouldnt it be an owning object?
					// in case we want to use owners function to set the property
					functionContext.object = context.property->GetOwnerAddres(context.objectStart);
					setter->Invoke(functionContext);
				}
				else
				{
					memcpy(context.objectStart, tempVarriable, (uint32)typeSize);
				}
			}
		}

		memory::FrameMemoryPool::Get().Free({ tempVarriable, (uint32)typeSize });
#endif // RTTI_ENABLE_DRAWING
	}

	bool TypeDrawerBase::DrawHeader(TypeDrawingContext& context) const
	{
#ifdef RTTI_ENABLE_DRAWING
		editor::DrawLabel(context.GetDisplayName(), context.offset);
		ImGui::SameLine();
#endif // RTTI_ENABLE_DRAWING
		return true;
	}

	bool TypeDrawerBase::DrawValue(TypeDrawingContext& context) const
	{
#ifdef RTTI_ENABLE_DRAWING
		if (context.readOnly)
		{
			ImGui::BeginDisabled();
		}

		bool changed = DrawType(context);

		if (context.readOnly)
		{
			RE_ASSERT(changed == false);
			ImGui::EndDisabled();
		}

		return changed; 
#else
        return false;
#endif // RTTI_ENABLE_DRAWING
	}

	bool TypeDrawerBase::Draw(TypeDrawingContext& context, std::function<void()> afterLabelCall ) const
	{
#ifdef RTTI_ENABLE_DRAWING
		bool showContents = true;
		bool changed = false;
		if (context.drawHeader)
		{
			showContents = DrawHeader(context);
			afterLabelCall();
		}
		
		if (showContents)
		{
			changed = DrawValue(context);
		}

		if (showContents && context.drawHeader)
		{
			EndHeader(context);
		}

		return changed;
#else
        return false;
#endif // RTTI_ENABLE_DRAWING
	}



	bool CompoundTypeDrawer::DrawType(TypeDrawingContext& context) const
	{
#ifdef RTTI_ENABLE_DRAWING
		ImGuiTreeNodeFlags flags = ImGuiTreeNodeFlags_AllowItemOverlap
			| ImGuiTreeNodeFlags_DefaultOpen
			| ImGuiTreeNodeFlags_OpenOnArrow;

		bool anyChanged = false;
		bool first = true;;
		int categoryId = 0;
		for (auto category : _categories)
		{
			ImGuiPushID categoryPushId(++categoryId);

			if (!first)
			{
				ImGui::NewLine();
				first = false;
			}

			bool open = true;
			bool hasCategory = strcmp("", category) != 0;
			if (hasCategory)
			{
				ImGui::SetCursorPosX(ImGui::GetCursorPosX() + context.offset * editor::LABEL_OFFSET_STEP);
				open = ImGui::CollapsingHeader(category, flags);
			}

			int i = 0;
			if (open)
			{
				for (auto& entry : _typeDrawingEntries)
				{
					if (strcmp(entry->GetPropertyCategory(), category) == 0)
					{
						TypeDrawingContext entryContext(context);
						if (hasCategory)
						{
							++entryContext.offset;
						}

						ImGuiPushID id(i++);

						anyChanged |= entry->Draw(entryContext);
					}
				}
			}

		}
		return anyChanged;
#else
    return false;
#endif // RTTI_ENABLE_DRAWING
	}
	void CompoundTypeDrawer::AddDrawer(TypeDrawerBase* drawer)
	{
		_typeDrawingEntries.push_back(drawer);

		// set default category
		drawer->SetPropertyCategory(GetPropertyCategory());
	}
	void CompoundTypeDrawer::RemoveDrawer(TypeDrawerBase* drawer)
	{
		auto found = std::find(_typeDrawingEntries.begin(), _typeDrawingEntries.end(), drawer);
		if (found != _typeDrawingEntries.end())
		{
			_typeDrawingEntries.erase(found);
			PrepareCategories();
		}
	}
	void CompoundTypeDrawer::PrepareCategories()
	{
		_categories.clear();
		for (auto& entry : _typeDrawingEntries)
		{
			bool categoryAlreadyDefined = false;
			for (auto category : _categories)
			{
				if (strcmp(category, entry->GetPropertyCategory()) == 0)
				{
					categoryAlreadyDefined = true;
					break;
				}
			}
			if (categoryAlreadyDefined)
				continue;

			_categories.push_back(entry->GetPropertyCategory());
		}
	}
	const char* TypeDrawingContext::GetDisplayName() const
	{
		return property ? property->GetDisplayName() : fieldName;
	}
}
