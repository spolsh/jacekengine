#pragma once

template<typename T>
void Log(T arg)
{
  std::cout << arg << '\n';
}

template<typename T>
void SafeRelease(T*& comObject)
{
  if (comObject)
  {
    comObject->Release();
    comObject = nullptr;
  }
}

template<typename T>
class AddSpace
{
private:
  T const& ref;
public:
  AddSpace(T const& r) : ref(r)
  {
  }
  friend std::ostream& operator<< (std::ostream& os, AddSpace<T> s)
  {
    return os << s.ref << ' ';
  }
};

template<typename... Types>
void Log(Types const&... args)
{
  (std::cout << ... << AddSpace(args)) << '\n';
}

template<typename T, unsigned int Size>
struct TStaticArray
{
  T array[Size];
};

template<unsigned int Width, unsigned int Height>
struct TResolution
{
  unsigned int width = Width;
  unsigned int height = Height;
};
