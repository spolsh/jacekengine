#pragma once
#include "../Type/rttiTypeBase.h"
#include "../Class/rttiProperty.h"
#include "../Function/rttiFunction.h"
#include "../Class/rttiInterface.h"


namespace rtti
{
	class ClassType : public TypeBase, public CompoundTypeDrawer, public ITypeSerializerBase
	{
	public:
		ClassType(const char* name, uint16 size);

	public: // Inheritance

		/// /brief sets base type of the class
		virtual void SetBaseType(ClassType* baseType)
		{
			_baseType = baseType;
			if (baseType)
			{
				for (auto& baseProperty : baseType->_properties)
				{
					_properties.push_back(baseProperty);
				}
				for (auto& baseDrawer : baseType->_typeDrawingEntries)
				{
					_typeDrawingEntries.push_back(baseDrawer);
				}
				for (auto& interface : baseType->_interfaces)
				{
					_interfaces.push_back(interface);
					interface->AddDerived(this);
				}
				for (auto& memberFunction : baseType->_memberFunctions)
				{
					_memberFunctions.push_back(memberFunction);
				}

				baseType->AddDerived(this);
			}
		}
		
		/// /brief returns base type of the class
		ClassType* GetBaseType() const { return _baseType; }

		/// /brief sets if the class is or isn't abstract
		///
		/// abstract classes wont apear in editor
		/// TODO think about make the function protected
		void SetAsAbstract(bool isAbstract)
		{
			_isAbstract = isAbstract;
		}

		/// /brief returns if the class is declared as and abstract one
		NO_DISCARD bool IsAbstract() const { return _isAbstract; }

		/// /brief sets if the class is or isn't abstract
		///
		/// abstract classes wont apear in editor
		/// TODO think about make the function protected
		void SetAsPolimorphic(bool isPolimorphic)
		{
			_isPolimorphic = isPolimorphic;
		}

		/// /brief returns if the class is declared as and abstract one
		NO_DISCARD bool IsPolimorphic() const { return _isPolimorphic; }

		/// /brief returns iterator of directly deriving classes from this one
		///
		/// usefull for iterating over class types
		NO_DISCARD TypeIterator<TDynArray<ClassType*>> GetDirectlyDerivedIterator() const
		{
			return TypeIterator<TDynArray<ClassType*>>(_derived);
		}

		/// /brief returns iterator of (directly or not) deriving classes from this one
		///
		/// usefull for iterating over class types
		NO_DISCARD TypeIterator<TDynArray<ClassType*>> GetAllDerivedIterator() const
		{
			return TypeIterator<TDynArray<ClassType*>>(_derivedAll);
		}

		/// /brief searches over all derived types one with a given native name
		NO_DISCARD ClassType* FindSubclassWithNativeName(const char* nativeName)
		{
			if (strcmp(GetNativeName(), nativeName) == 0)
			{
				return this;
			}
			else
			{
				for (auto _it : GetAllDerivedIterator())
				{
					if (strcmp(_it->GetNativeName(), nativeName) == 0)
					{
						return _it;
					}
				}
			}

			return nullptr;
		}

		/// /brief searches over all derived types one with a given display name
		NO_DISCARD ClassType* FindSubclassWithDisplayName(const char* displayName)
		{
			if (strcmp(GetDisplayName(), displayName) == 0)
			{
				return this;
			}
			else
			{
				for (auto _it : GetAllDerivedIterator())
				{
					if (strcmp(_it->GetDisplayName(), displayName) == 0)
					{
						return _it;
					}
				}
			}

			return nullptr;
		}

		/// /brief searches over directly derived types one with a given native name
		NO_DISCARD ClassType* FindDirectSubclassWithNativeName(const char* nativeName)
		{
			if (strcmp(GetNativeName(), nativeName) == 0)
			{
				return this;
			}
			else
			{
				for (auto _it : GetDirectlyDerivedIterator())
				{
					if (strcmp(_it->GetNativeName(), nativeName) == 0)
					{
						return _it;
					}
				}
			}

			return nullptr;
		}

		/// /brief searches over directly derived types one with a given display name
		NO_DISCARD ClassType* FindDirectSubclassWithDisplayName(const char* displayName)
		{
			if (strcmp(GetDisplayName(), displayName) == 0)
			{
				return this;
			}
			else
			{
				for (auto _it : GetDirectlyDerivedIterator())
				{
					if (strcmp(_it->GetDisplayName(), displayName) == 0)
					{
						return _it;
					}
				}
			}

			return nullptr;
		}

		/// /brief checks if class is or is deriving from type @Ty
		template<typename Ty>
		NO_DISCARD bool IsA() const;

		/// /brief checks if class is or is deriving from type @otherType
		NO_DISCARD bool IsA(const ClassType* otherType) const
		{
			if (otherType == this)
				return true;
			if (_baseType == nullptr)
				return false;
			if (_baseType == otherType)
				return true;

			return _baseType->IsA(otherType);
		}

		NO_DISCARD bool IsA(const InterfaceType* otherType) const
		{
			for (int i = 0; i < _interfaces.size(); ++i)
			{
				if ( _interfaces[i] == otherType)
				{
					return _interfaces[i];
				}
			}
			return false;
		}

		/// /brief Returns archetype of this class
		///
		/// Archetype is a class that could be used to initialize object of a given class
		/// You can think of it as a set of parameters that define an object
		/// Concept of Archetypes is similar to Prefabs but the difference is that Archetype doesn't have to be an instance of the class itself
		NO_DISCARD ClassType* GetArchetype() const { return _archetype; }

		/// /brief Sets archetype of this class
		///
		/// Archetype is a class that could be used to initialize object of a given class
		/// You can think of it as a set of parameters that define an object
		/// Concept of Archetypes is similar to Prefabs but the difference is that Archetype doesn't have to be an instance of the class itself
		void SetArchetype(ClassType* archetype) { _archetype = archetype; }

		/// /brief Returns a class this type is an archetype for
		///
		/// Archetype is a class that could be used to initialize object of a given class
		/// You can think of it as a set of parameters that define an object
		/// Concept of Archetypes is similar to Prefabs but the difference is that Archetype doesn't have to be an instance of the class itself
		NO_DISCARD ClassType* GetArchetypeFor() const { return _archetypeFor; }

		/// /brief Marks this class as an archetype for a class
		///
		/// Archetype is a class that could be used to initialize object of a given class
		/// You can think of it as a set of parameters that define an object
		/// Concept of Archetypes is similar to Prefabs but the difference is that Archetype doesn't have to be an instance of the class itself
		void SetArchetypeFor(ClassType* archetypeFor) { _archetypeFor = archetypeFor; }

	public: ////// Properties

		NO_DISCARD TypeIterator<TDynArray<std::shared_ptr<Property>>> GetPropertyIterator() const { return { _properties }; }

		/// /brief returns ith property of the class
		NO_DISCARD Property* GetProperty(size_t i)
		{
			if (i >= _properties.size())
			{
				return nullptr;
			}

			return _properties[i].get();
		}

		/// /brief returns ith property of the class
		NO_DISCARD Property* GetProperty(size_t i) const
		{
			if (i >= _properties.size())
			{
				return nullptr;
			}

			return _properties[i].get();
		}

		/// /brief returns property having native name @nativeName
		NO_DISCARD Property* FindPropertyByNativeName(const char* nativeName)
		{
			for (int i = 0; i < _properties.size(); ++i)
			{
				if (strcmp(nativeName, _properties[i]->GetNativeName()) == 0)
				{
					return _properties[i].get();
				}
			}
			return nullptr;
		}

		/// /brief returns property having native name @nativeName
		NO_DISCARD Property* FindPropertyByNativeName(const char* nativeName) const
		{
			for (int i = 0; i < _properties.size(); ++i)
			{
				if (strcmp(nativeName, _properties[i]->GetNativeName()) == 0)
				{
					return _properties[i].get();
				}
			}
			return nullptr;
		}

		/// /brief returns property having display name @displayName
		NO_DISCARD Property* FindPropertyByDisplayName(const char* displayName)
		{
			for (int i = 0; i < _properties.size(); ++i)
			{
				if (strcmp(displayName, _properties[i]->GetDisplayName()) == 0)
				{
					return _properties[i].get();
				}
			}
			return nullptr;
		}

		/// /brief returns property having display name @displayName
		NO_DISCARD Property* FindPropertyByDisplayName(const char* displayName) const
		{
			for (int i = 0; i < _properties.size(); ++i)
			{
				if (strcmp(displayName, _properties[i]->GetDisplayName()) == 0)
				{
					return _properties[i].get();
				}
			}
			return nullptr;
		}

		/// /brief returns how many properties have this class declared
		size_t GetPropertyCount() const { return _properties.size(); }

		/// /brief declares a new property to the class
		///
		/// @returns added property value to allow chained setup of _it's parameters
		/// @see RTTI_PROPERTY macro, _it is adviced to use _it instead of directly calling the function
		Property& AddProperty(TypeBase* type, const char* nativeName, uint32 offset, bool isReadOnly = false, bool isReference = false)
		{
			for (int i = 0; i < _properties.size(); ++i)
			{
				if (strcmp(nativeName, _properties[i]->GetNativeName()) == 0)
				{
					// remove the old property drawer
					for (int j = 0; j < _typeDrawingEntries.size(); ++j)
					{
						if (_typeDrawingEntries[j] == _properties[i].get())
						{
							_typeDrawingEntries.erase(_typeDrawingEntries.begin() + j);
							break;
						}
					}

					// remove old property
					_properties.erase(_properties.begin() + i);
					break;
				}
			}

			_properties.push_back(std::make_shared<Property>(type, nativeName, this, offset, isReadOnly, isReference));
			AddDrawer(_properties.back().get());
			return *_properties.back();
		}

		void RemoveProperty(uint32 id)
		{
			RemoveDrawer(_properties[id].get());
			_properties.erase(_properties.begin() + id);	
		}


	public: // function

		NO_DISCARD TypeIterator<TDynArray<std::shared_ptr<FunctionTypeBase>>> GetMemberFunctionIterator() const { return { _memberFunctions }; }

		/// /brief declares a new function to the class
		/// 
		/// @returns added function value to allow chained setup _it's parameters
		/// @see RTTI_MEMBER_FUNCTION, _it is adviced to use the function instead of directly calling
		FunctionTypeBase* AddMemberFunction(FunctionTypeBase* function)
		{
			_memberFunctions.push_back(std::shared_ptr<FunctionTypeBase>(function));
			return function;
		}
		
		/// /brief returns id'th function of the class
		/// 
		/// @returns nullptr if id isn't in function number range
		NO_DISCARD FunctionTypeBase* GetMemberFunction(size_t id) const
		{
			if (id >= _memberFunctions.size())
			{
				return nullptr;
			}
			return _memberFunctions[id].get();
		}

		/// /brief returns function with @nativeName
		/// 
		/// @returns nullptr if there is no such a function
		NO_DISCARD FunctionTypeBase* FindMemberFunctionByNativeName(const char* nativeName)
		{
			for (int i = 0; i < _memberFunctions.size(); ++i)
			{
				if (strcmp(nativeName, _memberFunctions[i]->GetNativeName()) == 0)
				{
					return _memberFunctions[i].get();
				}
			}
			return nullptr;
		}

		/// /brief returns function with @displayName
		/// 
		/// @returns nullptr if there is no such a function
		NO_DISCARD FunctionTypeBase* FindMemberFunctionByDisplayName(const char* displayName)
		{
			for (int i = 0; i < _memberFunctions.size(); ++i)
			{
				if (strcmp(displayName, _memberFunctions[i]->GetDisplayName()) == 0)
				{
					return _memberFunctions[i].get();
				}
			}
			return nullptr;
		}

		/// /brief returns how many functions are declared to the class
		NO_DISCARD size_t GetFunctionCount() const { return _memberFunctions.size(); }

	public: // Interfaces

		void AddInterface(InterfaceType* interface)
		{
			_interfaces.push_back(interface);
		}

		NO_DISCARD const InterfaceType* FindInterfaceByDisplayName(const char* displayName) const
		{
			for (int i = 0; i < _interfaces.size(); ++i)
			{
				if (strcmp(displayName, _interfaces[i]->GetNativeName()) == 0)
				{
					return _interfaces[i];
				}
			}
			return nullptr;
		}

		NO_DISCARD InterfaceType* FindInterfaceByNativeName(const char* nativeName) const
		{
			for (int i = 0; i < _interfaces.size(); ++i)
			{
				if (strcmp(nativeName, _interfaces[i]->GetNativeName()) == 0)
				{
					return _interfaces[i];
				}
			}
			return nullptr;
		}

		NO_DISCARD InterfaceType* GetInterface(size_t id) const { return _interfaces[id]; }

		NO_DISCARD size_t GetInterfaceCount() const { return _interfaces.size(); }

		NO_DISCARD TypeIterator<TDynArray<InterfaceType*>> GetInterfaceIterator() const { return { _interfaces }; }

	protected: // Drawing

		virtual bool DrawHeader(TypeDrawingContext& context) const override;

		virtual bool DrawType(TypeDrawingContext& context) const override;

	public: // Serializing

		virtual void Serialize(YAML::Emitter& emitter, SerializationContext serializationContext) const override;
		virtual void Deserialize(const YAML::Node& node, SerializationContext serializationContext) const override;

	protected:

		void AddDerived(ClassType* derived)
		{
			if (std::find(_derived.begin(), _derived.end(), derived) != _derived.end())
				return; // already added;

			derived->_baseType = this;
			_derived.push_back(derived);
			AddToAllDerrived(derived);
		}
		void AddToAllDerrived(ClassType* derived)
		{
			if (std::find(_derivedAll.begin(), _derivedAll.end(), derived) != _derivedAll.end())
				return; // already added;
			_derivedAll.push_back(derived);

			if (_baseType)
			{
				_baseType->AddToAllDerrived(derived);
			}
		}
	
	private:
		TDynArray<std::shared_ptr<Property>> _properties;
		TDynArray<ClassType*> _derived;
		TDynArray<ClassType*> _derivedAll;
		TDynArray<InterfaceType*> _interfaces;
		TDynArray<std::shared_ptr<FunctionTypeBase>> _memberFunctions;

		ClassType*	_baseType;
		ClassType*	_archetype;
		ClassType*	_archetypeFor;

		bool _isAbstract = false;
		bool _isPolimorphic = false;

	};

	template<typename TyResult, typename TyVarriable>
		requires(std::is_convertible_v<TyVarriable, TyResult>
			&& std::is_reference_v<TyVarriable> == false
			&& std::is_const_v<TyVarriable> == false
			&& std::is_reference_v<TyResult> == false
			&& std::is_const_v<TyResult> == false
		)
	inline TyResult* Cast(TyVarriable* varriable)
	{
		if (varriable->GetType()->IsA<TyResult>())
		{
			return static_cast<TyResult*>(varriable);
		}
		return nullptr;
	}

	template<typename TyResult, typename TyVarriable>
		requires(std::is_convertible_v<TyVarriable, TyResult>
			&& std::is_reference_v<TyVarriable> == false
			&& std::is_const_v<TyVarriable> == false
			&& std::is_reference_v<TyResult> == false
			&& std::is_const_v<TyResult> == false
		)
	inline const TyResult* Cast(const TyVarriable* varriable)
	{
		if (varriable->GetType()->IsA<TyResult>())
		{
			return static_cast<const TyResult*>(varriable);
		}
		return nullptr;
	}

	/// /brief accessor of class types
	template<typename Type>
		requires(std::is_class_v<Type>)
	inline ClassType* GetClassDescriptor()
	{
		RE_ASSERT((dynamic_cast<ClassType*>(GetTypeDescriptor<Type>()) != nullptr));
		return (ClassType*)GetTypeDescriptor<Type>();
	}
}

#include "rttiClassType.inl"
