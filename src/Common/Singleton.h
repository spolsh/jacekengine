#pragma once
#include "NonCopyable.h"

template<typename Type>
class Singleton : protected NonCopyable
{
public:
	static Type& Get()
	{
		static Type instance;
		return instance;
	}
};

template<typename Type>
class GlobalPtr
{
public:
	static Type& Get()
	{
		return **_GetPtr();
	}

	static bool IsGlobalPtrSet()
	{
		return _GetPtr() != nullptr && *_GetPtr() != nullptr;
	}


protected:
	static void SetPtr(Type* obj)
	{
		*_GetPtr() = obj;
	}

	static Type** _GetPtr()
	{
		static Type* instance = nullptr;
		return &instance;
	}
};

#define OVERRIDE_GLOBAL_PTR(NewClass)	\
public:									\
	static NewClass& Get()				\
	{									\
		return (NewClass&)**_GetPtr();	\
	}
